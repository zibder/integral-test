//
// Created by AndreySH on 19.02.2020.
//

#include "integralclientonvif.h"

IntegralClientOnvif::IntegralClientOnvif() {}

QNetworkRequest IntegralClientOnvif::_subscribeRequest(QString requestUrl)
{
    QNetworkRequest networkRequest = QNetworkRequest(QUrl(requestUrl));
    networkRequest.setRawHeader("User-Agent", "Integral Monitor");
    networkRequest.setRawHeader("X-Custom-User-Agent", "Integral Monitor");

    networkRequest.setRawHeader("User-Agent", " Test/1.1");
    networkRequest.setRawHeader("Content-Type", "text/plain; charset=UTF-8");

    return networkRequest;
}

QString IntegralClientOnvif::_subscribeBody(const QString &topicExpression, const QString &clientAddress)
{
    return QString(ACTION_SUBSCRIBE_TEMPLATE)
        .replace(TEMPLATE_KEY__CLIENT_ADDRESS, clientAddress)
        .replace(TEMPLATE_KEY__TOPIC_EXPRESSION, topicExpression);
}

QNetworkRequest IntegralClientOnvif::neutronProbingDetectSubscribeRequest(QString requestUrl, QString clientAddress)
{
    QNetworkRequest request = _subscribeRequest(requestUrl);

    request.setRawHeader("Content-Length", QByteArray::number(neutronProbingDetectSubscribeBody(clientAddress).size()));

    return request;
}

QNetworkRequest IntegralClientOnvif::introscopyLimitExceededSubscribeRequest(QString requestUrl, QString clientAddress)
{
    QNetworkRequest request = _subscribeRequest(requestUrl);

    request.setRawHeader("Content-Length", QByteArray::number(introscopyLimitExceededSubscribeBody(clientAddress).size()));

    return request;
}

QByteArray IntegralClientOnvif::neutronProbingDetectSubscribeBody(QString clientAddress)
{
    return QByteArray::fromStdString(_subscribeBody(TOPIC__NEUTRON_PROBING__DETECT, clientAddress).toStdString());
}

QByteArray IntegralClientOnvif::introscopyLimitExceededSubscribeBody(QString clientAddress)
{
    return QByteArray::fromStdString(_subscribeBody(TOPIC__INTROSCOPY__LIMIT_EXCEEDED, clientAddress).toStdString());
}
