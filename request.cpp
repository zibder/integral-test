#include "request.h"

Request::Request()
{
    clientAddress="";
    expression="";
    extendParams="";
    body="";
    params={};

    /*
        Inner placeholders:
        #CLIENT_ADDRESS# -- http://127.0.0.1:18888
        #EXPRESSION#     -- tns1:Device/NeutronProbing/ExtendGetSessionId
        #EXTEND_PARAMS#         -- ExtendParamsJson
    */

    //protocol1
    /*
    tpl=""
        "<env:Envelope "
        "    xmlns:env=\"http://www.w3.org/2003/05/soap-envelope\" "
        "    xmlns:a=\"http://www.w3.org/2005/08/addressing\" "
        "    xmlns:s=\"http://www.w3.org/2003/05/soap-envelope\">"
        "    <env:Header>"
        "        <a:Action s:mustUnderstand=\"1\">http://docs.oasis-open.org/wsn/bw-2/NotificationProducer/SubscribeRequest</a:Action>"
        "        <a:To s:mustUnderstand=\"1\">http://127.0.0.1:9999/onvif</a:To>"
        "        <a:ReplyTo>"
        "           <a:Address>http://www.w3.org/2005/08/addressing/anonymous</a:Address>"
        "        </a:ReplyTo>"
        "    </env:Header>"
        "    <env:Body>"
        "        <ns2:Subscribe>"
        "            <ns2:ConsumerReference>"
        "                <Address>#CLIENT_ADDRESS#</Address>"
        "            </ns2:ConsumerReference>"
        "            <ns2:Filter>"
        "               <wsnt:TopicExpression Dialect=\"http://www.onvif.org/ver10/tev/topicExpression/ConcreteSet\" ExtendParamsJson=\"#EXTEND_PARAMS#\">"
        "                    #EXPRESSION#"
        "                </wsnt:TopicExpression>"
        "            </ns2:Filter>"
        "            <ns2:InitialTerminationTime>PT300S</ns2:InitialTerminationTime>"
        "        </ns2:Subscribe>"
        "    </env:Body>"
        "</env:Envelope>";
    */

    //protocol2
    tpl=""
        "<Envelope>"
            "<Header>"
                "<Action s:mustUnderstand=\"1\">VirtualChip/Advance</Action>"
                "<To s:mustUnderstand=\"1\"></To>"
                "<ReplyTo>"
                    "<Address>http://www.w3.org/2005/08/addressing/anonymous</Address>"
                "</ReplyTo>"
            "</Header>"
            "<Body>"
            "<ns2:Subscribe>"
                "<ns2:ConsumerReference>"
                    "<Address>#CLIENT_ADDRESS#</Address>"
                "</ns2:ConsumerReference>"
                "<ns2:Filter>"
                    "<wsnt:TopicExpression Dialect=\"\" AdvanceParamsJson=\"#EXTEND_PARAMS#\">"
                        "#EXPRESSION#"
                    "</wsnt:TopicExpression>"
                "</ns2:Filter>"
                "<ns2:InitialTerminationTime>PT300S</ns2:InitialTerminationTime>"
            "</ns2:Subscribe>"
            "</Body>"
            "</Envelope>";
}

QString Request::Build()
{
    QString result="";

    //params
    QJsonDocument jdoc(params);
    QString jstr(jdoc.toJson(QJsonDocument::Compact));
    jstr=jstr.replace("\"","&quot;");
    extendParams=jstr;

    body=tpl;
    body.replace( QString("#CLIENT_ADDRESS#"), QString(clientAddress) );
    body.replace( QString("#EXPRESSION#"), QString(expression) );
    body.replace( QString("#EXTEND_PARAMS#"), QString(extendParams) );
    //body.replace( QString(), QString() );

    result=body;

    return result;
}
