//
// Created by AndreySH on 19.02.2020.
//

#pragma once

#include "QtCore"
#include "QtNetwork"


class IntegralClientProcessingEventsOnvif : public QThread, public QMutex
{
    Q_OBJECT

private:
    enum {
        INTEGRAL_CLIENT_EVENTS__STATE__UNKNOWN = 0,
        INTEGRAL_CLIENT_EVENTS__STATE__CONTENT_LENGTH,
        INTEGRAL_CLIENT_EVENTS__STATE__WAIT_CONTENT,
        INTEGRAL_CLIENT_EVENTS__STATE__READ_CONTENT,
        INTEGRAL_CLIENT_EVENTS__STATE__FINISH
    };

    const QByteArray HTTP_END_LINE = "\r\n";

    const QByteArray HEADER__CONTENT_LENGTH = "Content-Length";

private:
    QTcpSocket* _socket;
    QByteArray _readBuffer, _lineBuffer, _contentBuffer;
    int _state, _contentLength;

private:
    void _processing(char ch);
    void _processingContentLength();
    void _processingWaitContent();
    void _processingReadContent();
    void _processingFinish();
    QByteArray _emptyResponse();

protected:
    void run() override;

public:
    explicit IntegralClientProcessingEventsOnvif(qintptr handle);
    ~IntegralClientProcessingEventsOnvif();

    void setSocketDescriptor(quintptr handle);

public slots:
    void slotReadyRead();
    void slotReadyWrite(QByteArray data);

signals:
    void emitNewMessage(QHash<QString, QString> data);
    void emitDisconnected();
    void emitReadyWrite(QByteArray data);
};