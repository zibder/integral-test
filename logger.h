#pragma once
#include <QDebug>
#include <QtCore>
#include <QString>
#include <iostream>

static const int LOG_DEBUG = 1;
static const int LOG_ERROR = 2;
static const int LOG_INFO = 3;
static const int LOG_TRACE = 4;
static const int LOG_TRACE2 = 5;

class Logger
{
public:
    Logger();
    void msg(QString,int);
    void msg(QString);
    QString getContent();
    QString getToday();
    int level;

private:
    QString content;

signals:

};


