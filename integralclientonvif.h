//
// Created by AndreySH on 19.02.2020.
//

#pragma once

#include <QtCore>
#include <QtNetwork>

class IntegralClientOnvif : public QObject
{
private:
    const QString ACTION_SUBSCRIBE_TEMPLATE =
        "<env:Envelope \n"
        "    xmlns:env=\"http://www.w3.org/2003/05/soap-envelope\" \n"
        "    xmlns:a=\"http://www.w3.org/2005/08/addressing\" \n"
        "    xmlns:s=\"http://www.w3.org/2003/05/soap-envelope\">"
        "<env:Header>\n"
        "    <a:Action "
        "s:mustUnderstand=\"1\">http://docs.oasis-open.org/wsn/bw-2/NotificationProducer/SubscribeRequest</a:Action>\n"
        "    <a:To s:mustUnderstand=\"1\">http://127.0.0.1:9999/onvif</a:To>\n"
        "    <a:ReplyTo>\n"
        "        <a:Address>http://www.w3.org/2005/08/addressing/anonymous</a:Address>\n"
        "    </a:ReplyTo>\n"
        "</env:Header>\n"
        "\n"
        "<env:Body>\n"
        "<ns2:Subscribe>\n"
        "    <ns2:ConsumerReference>\n"
        "        <Address>#CLIENT_ADDRESS#</Address>\n"
        "    </ns2:ConsumerReference>\n"
        "    <ns2:Filter>\n"
        "        <wsnt:TopicExpression Dialect=\"http://www.onvif.org/ver10/tev/topicExpression/ConcreteSet\">\n"
        "            #TOPIC_EXPRESSION#"
        "        </wsnt:TopicExpression>\n"
        "    </ns2:Filter>\n"
        "    <ns2:InitialTerminationTime>PT300S</ns2:InitialTerminationTime>\n"
        "</ns2:Subscribe>\n"
        "</env:Body>"
        "</env:Envelope>";

    const QString TEMPLATE_KEY__CLIENT_ADDRESS = "#CLIENT_ADDRESS#";
    const QString TEMPLATE_KEY__TOPIC_EXPRESSION = "#TOPIC_EXPRESSION#";

    const QString TOPIC__NEUTRON_PROBING__DETECT = "tns1:Device/NeutronProbing/Detect";
    const QString TOPIC__INTROSCOPY__LIMIT_EXCEEDED = "tns1:Device/Introscopy/LimitExceeded";

public:
    explicit IntegralClientOnvif();

private:
    static QNetworkRequest _subscribeRequest(QString requestUrl);
    QString _subscribeBody(const QString &topicExpression, const QString &clientAddress);

public:
    QNetworkRequest neutronProbingDetectSubscribeRequest(QString requestUrl, QString clientAddress);
    QNetworkRequest introscopyLimitExceededSubscribeRequest(QString requestUrl, QString clientAddress);
    QByteArray neutronProbingDetectSubscribeBody(QString clientAddress);
    QByteArray introscopyLimitExceededSubscribeBody(QString clientAddress);
};
