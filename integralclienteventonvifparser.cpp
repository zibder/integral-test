//
// Created by asherbakov on 27.11.2019.
//

#include "integralclienteventonvifparser.h"
#include "QtXml"

static const QString TAG_ENVELOPE = "ENVELOPE";
static const QString TAG_HEADER = "HEADER";
static const QString TAG_BODY = "BODY";
static const QString TAG_FAULT = "FAULT";

static const int DOM_SAVE_INDENT = 4;

// Todo: then burned
// static const char *const TAG_SUB = "SubscriptionReference";

static inline bool _nodeNameIs(const QDomNode &node, const QString &name)
{
    return node.nodeName().toUpper().contains(name);
}

static inline void _nodeWrite(const QDomNode &node, QString &str)
{
    QTextStream stream(&str);
    node.save(stream, DOM_SAVE_INDENT);
}

const QString &IntegralClientParserSoap::envelope() const
{
    return _envelope;
}

const QString &IntegralClientParserSoap::header() const
{
    return _header;
}

const QString &IntegralClientParserSoap::body() const
{
    return _body;
}

const QString &IntegralClientParserSoap::fault() const
{
    return _fault;
}

void IntegralClientParserSoap::parse(const QString &content)
{
    reset();

    QDomDocument *document = new QDomDocument();

    document->setContent(content);
    const QDomElement &root = document->documentElement();

    if (_nodeNameIs(root, TAG_ENVELOPE))
    {
        _nodeWrite(root, _envelope);
    }

    for (int i = 0; i < root.childNodes().size(); i++)
    {

        const QDomNode &node = root.childNodes().item(i);

        if (_nodeNameIs(node, TAG_HEADER))
        {
            _nodeWrite(node, _header);
        }

        if (_nodeNameIs(node, TAG_BODY))
        {
            _nodeWrite(node, _body);

            for (int j = 0; i < node.childNodes().size(); j++)
            {
                const QDomNode &_subNode = node.childNodes().item(j);

                if (_nodeNameIs(_subNode, TAG_FAULT))
                {
                    _nodeWrite(_subNode, _fault);
                }
            }
        }
    }

    delete (document);
}

void IntegralClientParserSoap::reset()
{
    _content.clear();
    _envelope.clear();
    _header.clear();
    _body.clear();
    _fault.clear();
}
const QByteArray IntegralClientParserSoap::response(const QString &head, const QString &body)
{
    QByteArray result;

    result.append("<env:Envelope xmlns:env=\"http://www.w3.org/2003/05/soap-envelope\"> \n")
        .append("<env:Header>\n")
        .append(head)
        .append(" </env:Header>\n")
        .append(" <env:Body>\n")
        .append(body)
        .append(" </env:Body>\n")
        .append("</env:Envelope>");

    return result;
}

IntegralClientParserEventMessage::IntegralClientParserEventMessage(QString soapBody)
{
    QDomDocument *document = new QDomDocument();

    document->setContent(soapBody);
    const QDomElement &root = document->documentElement();

    _parseTagNotify(root.childNodes());

    document->clear();
    delete (document);
}

bool IntegralClientParserEventMessage::nodeNameIs(const QDomNode &node, const QString &name)
{
    return node.nodeName().toUpper().contains(name.toUpper());
}

void IntegralClientParserEventMessage::_parseTagNotify(QDomNodeList list)
{

    for (int i = 0; i < list.size(); i++)
    {
        const QDomNode &node = list.item(i);

        if (nodeNameIs(node, TAG__NOTIFY))
        {
            _parseTagNotifyNotificationMessage(node.childNodes());
        }
    }
}

void IntegralClientParserEventMessage::_parseTagNotifyNotificationMessage(QDomNodeList list)
{
    for (int i = 0; i < list.size(); i++)
    {
        const QDomNode &node = list.item(i);

        if (nodeNameIs(node, TAG__NOTIFY__NOTIFICATION_MESSAGE))
        {
            _parseTagNotifyNotificationMessageTopic(node.childNodes());
            _parseTagNotifyNotificationMessageMessage(node.childNodes());
        }
    }
}

void IntegralClientParserEventMessage::_parseTagNotifyNotificationMessageTopic(QDomNodeList list)
{
    for (int i = 0; i < list.size(); i++)
    {
        const QDomNode &node = list.item(i);

        if (nodeNameIs(node, TAG__NOTIFY__NOTIFICATION_MESSAGE__TOPIC))
        {
            _topic = node.toElement().text().replace("\r\n", "").replace("\n", "").replace("\r", "").replace(" ", "");
        }
    }
}

void IntegralClientParserEventMessage::_parseTagNotifyNotificationMessageMessage(QDomNodeList list)
{
    for (int i = 0; i < list.size(); i++)
    {
        const QDomNode &node = list.item(i);

        if (nodeNameIs(node, TAG__NOTIFY__NOTIFICATION_MESSAGE__MESSAGE))
        {
            _parseTagNotifyNotificationMessageMessageMessage(node.childNodes());
        }
    }
}

void IntegralClientParserEventMessage::_parseTagNotifyNotificationMessageMessageMessage(QDomNodeList list)
{
    for (int i = 0; i < list.size(); i++)
    {
        const QDomNode &node = list.item(i);

        if (nodeNameIs(node, TAG__NOTIFY__NOTIFICATION_MESSAGE__MESSAGE__MESSAGE))
        {
            _utcTime = node.attributes().namedItem(ATTR__UTC_TIME).nodeValue();

            _parseTagNotifyNotificationMessageMessageMessageSource(node.childNodes());
            _parseTagNotifyNotificationMessageMessageMessageData(node.childNodes());
        }
    }
}

void IntegralClientParserEventMessage::_parseTagNotifyNotificationMessageMessageMessageSource(QDomNodeList list)
{
    for (int i = 0; i < list.size(); i++)
    {
        const QDomNode &node = list.item(i);

        if (nodeNameIs(node, TAG__NOTIFY__NOTIFICATION_MESSAGE__MESSAGE__MESSAGE__SOURCE))
        {
            _parseTagNotifyNotificationMessageMessageMessageSourceSimpleItem(node.childNodes());
        }
    }
}

void IntegralClientParserEventMessage::_parseTagNotifyNotificationMessageMessageMessageSourceSimpleItem(QDomNodeList list)
{
    for (int i = 0; i < list.size(); i++)
    {
        const QDomNode &node = list.item(i);

        if (nodeNameIs(node, TAG__NOTIFY__NOTIFICATION_MESSAGE__MESSAGE__MESSAGE__SOURCE__SIMPLE_ITEM))
        {
            if (node.attributes().namedItem(ATTR__NAME).nodeValue().contains(VALUE_ATTR__ID))
            {
                _id = node.attributes().namedItem(ATTR__VALUE).nodeValue();
            }
        }
    }
}

void IntegralClientParserEventMessage::_parseTagNotifyNotificationMessageMessageMessageData(QDomNodeList list)
{
    for (int i = 0; i < list.size(); i++)
    {
        const QDomNode &node = list.item(i);

        if (nodeNameIs(node, TAG__NOTIFY__NOTIFICATION_MESSAGE__MESSAGE__MESSAGE__DATA))
        {
            _parseTagNotifyNotificationMessageMessageMessageDataSimpleItem(node.childNodes());
        }
    }
}

void IntegralClientParserEventMessage::_parseTagNotifyNotificationMessageMessageMessageDataSimpleItem(QDomNodeList list)
{
    for (int i = 0; i < list.size(); i++)
    {
        const QDomNode &node = list.item(i);

        if (nodeNameIs(node, TAG__NOTIFY__NOTIFICATION_MESSAGE__MESSAGE__MESSAGE__DATA__SIMPLE_ITEM))
        {
            QString name = node.attributes().namedItem(ATTR__NAME).nodeValue();
            QString value = node.attributes().namedItem(ATTR__VALUE).nodeValue();

            _data[name] = value;
        }
    }
}

QHash<QString, QString> IntegralClientParserEventMessage::data()
{
    _data[DATA_KEY__TOPIC] = _topic;
    _data[DATA_KEY__UTC_TIME] = _utcTime;
    _data[DATA_KEY__ID] = _id;

    return _data;
}