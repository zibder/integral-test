//
// Created by asherbakov on 27.11.2019.
//

#pragma once

#include <QtCore>
#include <QtXml>

class IntegralClientParserSoap
{
    QString _content;
    QString _envelope;
    QString _header;
    QString _body;
    QString _fault;

public:
    const QString &envelope() const;
    const QString &header() const;
    const QString &body() const;
    const QString &fault() const;

    const QByteArray response(const QString &head = "", const QString &body = "");

    void reset();

public:
    void parse(const QString &content);
};

class IntegralClientParserEventMessage
{
private:
    const QString TAG__NOTIFY = "Notify";
    const QString TAG__NOTIFY__NOTIFICATION_MESSAGE = "NotificationMessage";
    const QString TAG__NOTIFY__NOTIFICATION_MESSAGE__TOPIC = "Topic";
    const QString TAG__NOTIFY__NOTIFICATION_MESSAGE__MESSAGE = "Message";
    const QString TAG__NOTIFY__NOTIFICATION_MESSAGE__MESSAGE__MESSAGE = "Message";
    const QString TAG__NOTIFY__NOTIFICATION_MESSAGE__MESSAGE__MESSAGE__SOURCE = "Source";
    const QString TAG__NOTIFY__NOTIFICATION_MESSAGE__MESSAGE__MESSAGE__SOURCE__SIMPLE_ITEM = "SimpleItem";
    const QString TAG__NOTIFY__NOTIFICATION_MESSAGE__MESSAGE__MESSAGE__DATA = "Data";
    const QString TAG__NOTIFY__NOTIFICATION_MESSAGE__MESSAGE__MESSAGE__DATA__SIMPLE_ITEM = "SimpleItem";

    const QString ATTR__UTC_TIME = "UtcTime";
    const QString ATTR__NAME = "Name";
    const QString ATTR__VALUE = "Value";

    const QString VALUE_ATTR__ID = "Id";

    const QString DATA_KEY__ID = "Id";
    const QString DATA_KEY__UTC_TIME = "UtcTime";
    const QString DATA_KEY__TOPIC = "Topic";

private:
    inline void _parseTagNotify(QDomNodeList list);
    inline void _parseTagNotifyNotificationMessage(QDomNodeList list);
    inline void _parseTagNotifyNotificationMessageTopic(QDomNodeList list);
    inline void _parseTagNotifyNotificationMessageMessage(QDomNodeList list);
    inline void _parseTagNotifyNotificationMessageMessageMessage(QDomNodeList list);
    inline void _parseTagNotifyNotificationMessageMessageMessageSource(QDomNodeList list);
    inline void _parseTagNotifyNotificationMessageMessageMessageSourceSimpleItem(QDomNodeList list);
    inline void _parseTagNotifyNotificationMessageMessageMessageData(QDomNodeList list);
    inline void _parseTagNotifyNotificationMessageMessageMessageDataSimpleItem(QDomNodeList list);

private:
    QString _topic;
    QString _utcTime;
    QString _id;
    QHash<QString, QString> _data;

public:
    QHash<QString, QString> data();

private:
    static inline bool nodeNameIs(const QDomNode &node, const QString &name);

public:
    explicit IntegralClientParserEventMessage(QString soapBody);
};