#pragma once

#include <QtCore>

class Auth
{
public:
    Auth();
    void init();
    QString getMessages();
    bool checkAuth(QString,QString);
    QJsonObject user;
    QJsonObject getAccounts();
    void setAccounts(QJsonObject);
    int accountsCnt;

private:
    QString accountsFileName;
    QString messages;
    QJsonObject accounts;


signals:

};


