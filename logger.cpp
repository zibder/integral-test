#include "logger.h"

Logger::Logger()
{
    content="";
    level=0;
}

void Logger::msg(QString message, int type)
{
    QString p="    ";
    if(type==LOG_ERROR){
        p="[!] ";
    }

    content=content.append("\n");
    content=content.append(p);
    content=content.append(getToday());
    content=content.append(" ");
    content=content.append(message);

    if( level==1 ){
        if(type==LOG_INFO){
            qInfo()<<message.replace("\"","");
        }
    }

    if( level==2 ){
        if(type==LOG_INFO){
            qInfo()<<message.replace("\"","");
        }
        if(type==LOG_TRACE){
            qInfo()<<message.replace("\"","");
        }
    }

    if( level==3 ){
        if(type==LOG_INFO){
            qInfo()<<message.replace("\"","");
        }
        if(type==LOG_TRACE){
            qInfo()<<message.replace("\"","");
        }
        if(type==LOG_TRACE2){
            qInfo()<<message.replace("\"","");
        }
    }




    //qDebug()<< QString(message);
    //std::cout << QString::fromUtf8(message.toUtf8()).toLocal8Bit().data() << std::endl;

    //if(type==LOG_ERROR){
    //    qDebug()<<message;
    //}
}

void Logger::msg(QString message)
{
    msg(message,LOG_DEBUG);
}

QString Logger::getContent()
{
    return content;
}

QString Logger::getToday()
{
    QDate dt = QDate::currentDate();
    QTime tm = QTime::currentTime();

    QString dttm="";
    dttm=dttm.append(dt.toString("yyyy.MM.dd"));
    dttm=dttm.append(" ");
    dttm=dttm.append(tm.toString("hh:mm:ss"));

    return dttm;
}
