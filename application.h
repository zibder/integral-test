#pragma once


//#include <QtQuick/QQuickView>
#include <QObject>

#include <QtCore>
#include "central.h"
#include "monitor.h"

class Application : public QObject{
    Q_OBJECT

public:
    explicit Application(QObject *parent = nullptr);
    void run(char *argv[]);
    void bench();
    void showMonitor();
    void runMonitor();
    void showLogWindow();
    void showLoginWindow();
    void report();

    QList<Monitor*> clients;
    int clientsCnt;
    int clientsComplete;
    int queriesMax;
    QTimer *checkTimer;

    int queriesTotal;
    int errorsTotal;
    int maxQueries;

    QString serverAddress;
    QString clientPort;

private:
    Central* central;

public slots:
    void authCompleteSlot();
    void checkTimerSlot();

};


