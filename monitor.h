#ifndef MONITOR_H
#define MONITOR_H

#include <QObject>
#include <QVariant>
#include <QTimer>
#include <QTime>
#include "integralclient.h"
#include "central.h"

//query types
static const int QUERY_GET_SESSION = 0;
static const int QUERY_GET_DEVICES = 1;
static const int QUERY_GET_MESSAGES = 2;

static const int QUERY2_INIT            = 0;
static const int QUERY2_GET_SETTINGS    = 1;
static const int QUERY2_SUBSCRIBE       = 2;
static const int QUERY2_RENEW           = 3;
static const int QUERY2_UNSUBSCRIBE     = 4;
static const int QUERY2_GET_ARCHIVE     = 5;
static const int QUERY2_GET_ARCHIVE_FILTERED = 6;
static const int QUERY2_GET_ARCHIVE_IMAGE = 7;
static const int QUERY2_END             = 99;


//query topics
static const QString TOPIC_GET_SESSION="tns1:Device/NeutronProbing/ExtendGetSessionId";
static const QString TOPIC_GET_DEVICES="tns1:Device/NeutronProbing/ExtendGetSettings";
static const QString TOPIC_GET_MESSAGES="tns1:Device/NeutronProbing/ExtendDetect";
static const QString TOPIC_GET_MESSAGES_RAPISCAN="tns1:Device/Introscopy/LimitExtendExceeded";

static const QString TOPIC2_INIT            ="tns1:Device/VirtualChip/Init";
//static const QString TOPIC2_INIT            ="tns1:device/virtualchip/init";
static const QString TOPIC2_GET_SETTINGS    ="tns1:Device/VirtualChip/GetSettings";
static const QString TOPIC2_SUBSCRIBE       ="tns1:Device/VirtualChip/Subscribe";
static const QString TOPIC2_RENEW           ="tns1:Device/VirtualChip/Renew";
static const QString TOPIC2_UNSUBSCRIBE     ="tns1:Device/VirtualChip/Unsubscribe";
static const QString TOPIC2_GET_ARCHIVE     ="tns1:Device/VirtualChip/GetArchive";
static const QString TOPIC2_GET_ARCHIVE_FILTERED     ="tns1:Device/VirtualChip/GetArchiveFiltered";
static const QString TOPIC2_GET_ARCHIVE_IMAGE     ="tns1:Device/VirtualChip/GetArchiveImage";


//device types
static const int DEVICE_NEUTRON_PROBING_KERBER = 1;
static const int DEVICE_NEUTRON_PROBING_CHEMICAL_EXPERT = 2;
static const int DEVICE_INTROSCOPY_RAPISCAN_60XX = 3;

/*
    typeId
    1 видеонаблюдение
    2 скуд
    3 Пожарная сигнализация
    4 интроскопия
    5 газовый анализ
    6 радиационный контроль
    7 обнаружение взрывчатых веществ                    neutron_probing
    8 металлодетектор
    9 обнаружение паров и следов взрывчатых веществ

    vendorId
    1 kerber
    2 chemical expert
    3 rapiscan
*/

static const int DEVICE2_VENDOR_KERBER = 1;
static const int DEVICE2_VENDOR_CHEMICAL_EXPERT = 2;
static const int DEVICE2_VENDOR_RAPISCAN = 3;

static const int DEVICE2_TYPE_NEUTRON_PROBING = 7;
static const int DEVICE2_TYPE_INTROSCOPY = 4;


class Monitor : public QObject
{
    Q_OBJECT
public:
    explicit Monitor(QObject *parent = nullptr );
    //explicit Monitor(QQmlApplicationEngine *engine=nullptr, QObject *parent = nullptr );
    //explicit Monitor(QQmlApplicationEngine& engine, QObject *parent = nullptr);
    //Q_INVOKABLE static void test2();
    //Q_INVOKABLE  void test2();

    QString title;

    void testUpdateDevices();
    void testUpdateMessages();
    void run();

    bool stateConnected;
    int stateMessages;
    int stateDevices;

    QString clientPort;
    QString clientOnvifPort;
    int queriesComplete;
    int queriesError;
    int queriesCnt;
    int queriesMax;
    bool complete;
    QString name;

    int req;
    int ret;

private:
    QObject *root;
    Central* central;
    //QQmlApplicationEngine *engine;
    QTimer *anonceTimer;
    QTimer *deviceTimer;    
    QTimer *initTimer;
    QTimer *renewTimer;
    QTimer *waitResponceTimer;

    IntegralClient *client;
    void sendQuery(int);
    void setClientParams();
    bool sessionInitialized;

    QJsonObject devices;
    int devicesCnt;
    int messagesCnt;
    int currentDeviceIndex;
    int currentMessageIndex;
    QJsonObject currentDevice;

    bool waitingResponce;
    int currentQuery;
    void nextQuery();
    void mainLoop();
    void sendQuery2(int);
    bool firstFindingDevices;

    void runWorkingTimers();
    void runGetDevices();
    void logMessage(QString message);
    QJsonObject parseMessages(QJsonDocument,bool);
    void dumpMessages(QJsonObject);
    bool parseMessagesRealtime;
    QJsonObject parameters;
    void setParameters();

    QString archiveDateFrom;
    QString archiveDateTo;

    QString archiveImageSerialNumber;
    QString archiveImageFileName;
    QJsonObject getDeviceBySerialNumber(QString);

signals:
    //signals to QML
    void updateMessagesSignal(QString messages);
    void updateDevicesSignal(QString devices);
    void updateDeviceStateSignal(QString serialNumber,int status);
    void addMessage(QString message);
    void clearMessagesSignal();
    void setParametersSignal(QString parameters);
    void showImageSignal(QString fileName);



public slots:
    //void onTestSignal(const QString &msg);
    //void onTestUpdDevices();

    void anonceTimerSlot();
    void deviceTimerSlot();
    void initTimerSlot();
    void renewTimerSlot();
    void waitResponceTimerSlot();


    void showDbgWindowSlot();
    void showLogWindowSlot();
    void updateDevicesSlot();
    void updateMessagesSlot();
    void archiveModeOnSlot();
    void archiveModeOffSlot();
    void getArchiveSlot(const QString &dateFrom,const QString &dateTo);
    void getImageSlot(const QString &sn,const QString &fn);
    void showUsersWindowSlot();

    void slotNewMessage(QHash<QString, QString> data);


private slots:

    void onSendQuery(QString&);
    void processResponse(QString&);
    void processResponseXml(QHash<QString, QString>);


};

#endif // MONITOR_H
