#include "application.h"
#include <QCommandLineParser>

Application::Application(QObject *parent)
    : QObject(parent)

{
    central=Central::instance();
    central->init();

    checkTimer = new QTimer();
    connect(checkTimer, SIGNAL(timeout()), this, SLOT(checkTimerSlot()));

    queriesMax=0;
    clientsCnt=0;

    clientsComplete=0;
    clients.empty();

    queriesTotal=0;
    errorsTotal=0;
    maxQueries=0;

}



void Application::bench(){

    QCommandLineParser parser;
    QCoreApplication::setApplicationName("test_users");
    parser.addHelpOption();

    QCommandLineOption debugParam(QStringList() << "d" << "debug",
            QCoreApplication::translate("main", "Debug level"),
            QCoreApplication::translate("main", "int: 1..2"));
    parser.addOption(debugParam);

    QCommandLineOption usersParam(QStringList() << "u" << "users",
            QCoreApplication::translate("main", "Users quantity"),
            QCoreApplication::translate("main", "users"));
    parser.addOption(usersParam);

    QCommandLineOption requestsParam(QStringList() << "r" << "requests",
            QCoreApplication::translate("main", "Requests per one user"),
            QCoreApplication::translate("main", "requests"));
    parser.addOption(requestsParam);

    QCommandLineOption serverAddressParam(QStringList() << "s" << "integral-server",
            QCoreApplication::translate("main", "Integral server IP address"),
            QCoreApplication::translate("main", "ip"));
    parser.addOption(serverAddressParam);

    QCommandLineOption serverPortParam(QStringList() << "p" << "integral-port",
            QCoreApplication::translate("main", "Integral server port (18888 by default)"),
            QCoreApplication::translate("main", "port"));
    parser.addOption(serverPortParam);

    QCommandLineOption clientAddressParam(QStringList() << "c" << "host-ip",
            QCoreApplication::translate("main", "Current client IP address"),
            QCoreApplication::translate("main", "ip"));
    parser.addOption(clientAddressParam);

    QCommandLineOption clientPortParam(QStringList() << "f" << "host-port",
            QCoreApplication::translate("main", "Current client port"),
            QCoreApplication::translate("main", "ip"));
    parser.addOption(clientPortParam);

    parser.process(QCoreApplication::arguments());

    central->log->level = 1;
    QString debug = parser.value(debugParam);
    if( !debug.isEmpty() ){
        central->log->level = debug.toInt();
    }

    QString users = parser.value(usersParam);
    QString requests = parser.value(requestsParam);
    serverAddress = parser.value(serverAddressParam);
    QString serverPort = parser.value(serverPortParam);
    QString clientAddress = parser.value(clientAddressParam);
    clientPort = parser.value(clientPortParam);

    QString s="";
    s=s+" users:"+users;
    s=s+" requests:"+requests;
    s=s+" serverAddress:"+serverAddress;
    s=s+" serverPort:"+serverPort;
    s=s+" clientAddress:"+clientAddress;
    s=s+" clientPort:"+clientPort;
    central->log->msg(s,LOG_TRACE);

    central->serverAddress=serverAddress;
    central->serverPort=serverPort;
    central->clientAddress=clientAddress;

    queriesMax=requests.toInt();
    clientsCnt=users.toInt();

    //central->log->msg("starting clients="+QString::number(clientsCnt)+" max requests="+QString::number(queriesMax),LOG_INFO);

    //clients count: testing complete
    clientsComplete=0;
    clients.empty();

    queriesTotal=0;
    errorsTotal=0;
    maxQueries=clientsCnt*queriesMax;



    //central->parseArgs(argv);
    central->params["benchMode"]=true;

    int p=clientPort.toInt();

    if( clientsCnt > 0 ){
        for (int i=0;i<clientsCnt;i++) {

            Monitor *m=new Monitor;

            m->clientPort=QString::number(p);
            p++;
            m->clientOnvifPort=QString::number(p);
            p++;

            m->queriesMax=queriesMax;
            m->name="c"+QString::number(i+1);
            m->run();
            clients.append(m);

        }
    }

    checkTimer->start(3000);

}

void Application::checkTimerSlot(){

    QString s="    ";
    clientsComplete=0;

    queriesTotal=0;
    errorsTotal=0;


    if( clientsCnt > 0 ){
        for (int i=0;i<clientsCnt;i++) {

            Monitor *m;
            m=clients.at(i);

            s="    ";
            s=s+"    "+m->name+":complete="+QString::number(m->queriesCnt)+"/error="+QString::number(m->queriesError);
            s=s+" "+" req:"+QString::number(m->req)+" ret:"+QString::number(m->ret);
            central->log->msg(s,LOG_TRACE);



            queriesTotal=queriesTotal+m->queriesCnt;
            errorsTotal=errorsTotal+m->queriesError;

            if( m->complete ){
                clientsComplete++;
                //clients.removeAt(i);
                //central->log->msg("client complete:"+QString::number(i),LOG_INFO);
                //m->deleteLater();
            }
        }
    }

    //s=s+" clients:"+QString::number(clientsComplete)+" from:"+QString::number(clientsCnt);
    //central->log->msg(s,LOG_TRACE);

    if( queriesTotal >maxQueries ){
        queriesTotal=maxQueries;
    }

    s="";
    s=s+serverAddress+":"+clientPort+" ";
    s=s+"requests done:"+QString::number(queriesTotal)+"/"+QString::number(maxQueries)+", failed:"+QString::number(errorsTotal);
    central->log->msg(s,LOG_INFO);


    if( clientsComplete == clientsCnt){
        report();
    }

}

void Application::report()
{
    checkTimer->stop();

    //central->log->msg(" ",LOG_INFO);

    QString s="";
    s=s+serverAddress+":"+clientPort+" ";
    if( queriesTotal > queriesMax){
        queriesTotal=queriesMax;
    }
    s=s+"completed requests: "+QString::number(queriesTotal)+", failed requests:"+QString::number(errorsTotal);
    central->log->msg(s,LOG_INFO);

    //central->log->msg("all tests passed",LOG_INFO);

    QCoreApplication::quit();

}


void Application::run(char *argv[]){

}

void Application::authCompleteSlot()
{

}

void Application::showMonitor()
{

}

void Application::runMonitor()
{

}

void Application::showLoginWindow()
{

}

void Application::showLogWindow()
{

}

