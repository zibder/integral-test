//
// Created by AndreySH on 19.02.2020.
//

#include "integralclienteventsonvif.h"
#include "integralclienteventonvifparser.h"


IntegralClientProcessingEventsOnvif::IntegralClientProcessingEventsOnvif(qintptr handle)
    : _state(INTEGRAL_CLIENT_EVENTS__STATE__CONTENT_LENGTH), _socket(new QTcpSocket)
{
    connect(_socket, &QTcpSocket::readyRead, this, &IntegralClientProcessingEventsOnvif::slotReadyRead);
    connect(_socket, &QTcpSocket::disconnected, this, [=]() { emitDisconnected(); });

    connect(this, &IntegralClientProcessingEventsOnvif::emitReadyWrite, this, &IntegralClientProcessingEventsOnvif::slotReadyWrite);
}


IntegralClientProcessingEventsOnvif::~IntegralClientProcessingEventsOnvif()
{
    _socket->close();
    delete (_socket);
}

void IntegralClientProcessingEventsOnvif::setSocketDescriptor(quintptr handle)
{
    _socket->setSocketDescriptor(handle);
}

void IntegralClientProcessingEventsOnvif::slotReadyRead()


{
    lock();
    _readBuffer.append(_socket->readAll());
    unlock();

    start();
}

void IntegralClientProcessingEventsOnvif::run()
{

    lock();
    while (_readBuffer.size() > 0)
    {
        char ch = _readBuffer[0];
        _readBuffer.remove(0, 1);

        _processing(ch);
    }

    unlock();
}

void IntegralClientProcessingEventsOnvif::_processing(char ch)
{
    _lineBuffer.append(ch);
    _contentBuffer.append(ch);

    switch (_state)
    {
    case INTEGRAL_CLIENT_EVENTS__STATE__CONTENT_LENGTH: {
        if (_lineBuffer.contains(HTTP_END_LINE))
        {
            _processingContentLength();
            _lineBuffer.clear();
            _contentBuffer.clear();
        }
        break;
    }
    case INTEGRAL_CLIENT_EVENTS__STATE__WAIT_CONTENT: {
        if (_lineBuffer.contains(HTTP_END_LINE))
        {
            _processingWaitContent();
            _lineBuffer.clear();
            _contentBuffer.clear();
        }
        break;
    }
    case INTEGRAL_CLIENT_EVENTS__STATE__READ_CONTENT: {
        _processingReadContent();
        break;
    }
    case INTEGRAL_CLIENT_EVENTS__STATE__FINISH: {
        _processingFinish();
        break;
    }
    }
}

void IntegralClientProcessingEventsOnvif::_processingContentLength()
{
    if (_lineBuffer.contains(HEADER__CONTENT_LENGTH))
    {
        _contentLength = _lineBuffer.split(':')[1].trimmed().toUInt();
        _state = INTEGRAL_CLIENT_EVENTS__STATE__WAIT_CONTENT;
    }
}

void IntegralClientProcessingEventsOnvif::_processingWaitContent()
{
    if (_lineBuffer.size() == HTTP_END_LINE.size())
    {
        _state = INTEGRAL_CLIENT_EVENTS__STATE__READ_CONTENT;
    }
}

void IntegralClientProcessingEventsOnvif::_processingReadContent()
{
    if (_contentBuffer.size() == _contentLength)
    {
        _processingFinish();
    }
}

void IntegralClientProcessingEventsOnvif::_processingFinish()
{
    IntegralClientParserSoap soap;
    soap.parse(_contentBuffer);

    IntegralClientParserEventMessage eventMessage = IntegralClientParserEventMessage(soap.body());

    emitNewMessage(eventMessage.data());

    _readBuffer.clear();
    _contentBuffer.clear();


    emitReadyWrite(_emptyResponse());
}

QByteArray IntegralClientProcessingEventsOnvif::_emptyResponse()
{
    QByteArray result;

    result.append("HTTP/1.1 200 OK").append(HTTP_END_LINE);
    result.append("This-Is: EmptyResponse").append(HTTP_END_LINE);
    result.append("Content-Length: 0").append(HTTP_END_LINE);
    result.append("Connection: close").append(HTTP_END_LINE);

    return result;
}

void IntegralClientProcessingEventsOnvif::slotReadyWrite(QByteArray data)
{
    _socket->write(data);
    _socket->flush();
}