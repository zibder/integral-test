#include "central.h"


//singletone struct
Central::Central(): m_val(0){}
Central::Central(int val): m_val(val){}
int Central::get() const { return m_val; }
void Central::set(int val) { m_val=val; }
Central* Central::_instance=0;

Central* Central::instance()
{
    if(_instance == 0)
    {
        _instance=new Central;
    }
    return _instance;
}

//construct
void Central::init()
{
    buildName="Тест Интеграл";
    buildDate="2020-02-23";
    buildVersion="0.1.1";

    log=new Logger();
    log->msg("    "+buildName+" "+buildVersion+" "+buildDate);
    log->msg("    Запуск");

    auth=new Auth();
    auth->init();

    log->msg("    Авторизация");
    log->msg(auth->getMessages());


    configFile="application.cfg";

    //defaults
    serverAddress ="192.168.2.105";
    serverPort="9999";
    clientAddress="192.168.2.102";
    clientPort="18888";
    debugWindow=0;
    listenerTimeout=100;
    pathTemp="c:/temp/";
    params.empty();

    //reading config
    log->msg("    Чтение конфигурации: "+configFile);
    QFile file(configFile);
    if (!file.open(QIODevice::ReadOnly | QIODevice::Text)){        
        QString s="Файл конфигурации [%1] не найден.";
        s=s.arg(configFile);        
        log->msg(s, LOG_ERROR);
        return;
    }

    QString configData=file.readAll();
    configData=configData.replace("\t","").replace("\r","").replace("\n","");
    configData=configData.replace("\\","");

    file.close();

    QJsonDocument cfgDoc;
    if( !configData.isEmpty() ){        

        cfgDoc = QJsonDocument::fromJson(configData.toUtf8());

        QString s="";

        s=cfgDoc["server"]["address"].toString();
        if( !s.isEmpty() ){
            serverAddress=s;            
        }

        s=cfgDoc["server"]["port"].toString();
        if( !s.isEmpty() ){
            serverPort=s;            
        }

        s=cfgDoc["client"]["address"].toString();
        if( !s.isEmpty() ){
            clientAddress=s;
        }

        s=cfgDoc["client"]["port"].toString();
        if( !s.isEmpty() ){
            clientPort=s;
        }

        s=cfgDoc["client"]["onvifAddress"].toString();
        if( !s.isEmpty() ){
            clientOnvifAddress=s;
        }

        s=cfgDoc["client"]["onvifPort"].toString();
        if( !s.isEmpty() ){
            clientOnvifPort=s;
        }

        s=cfgDoc["debugWindow"].toString();
        if( !s.isEmpty() && s.toInt()==1 ){
            debugWindow=1;
        }

        s=cfgDoc["listenerTimeout"].toString();
        if( !s.isEmpty() ){
            listenerTimeout=s.toInt();
        }

        s=cfgDoc["tempFolder"].toString();
        if( !s.isEmpty() ){
            pathTemp=s;
        }

        QFileInfo info(pathTemp);
        if(info.isDir() && info.isWritable()){

            //clean up
            QDir dir(pathTemp);
            dir.setNameFilters(QStringList() << "*");
            dir.setFilter(QDir::Files);
            foreach(QString dirFile, dir.entryList())
            {
                dir.remove(dirFile);
            }

        }else{            
            QString s="Директория [%1] недоступна для записи.";
            s=s.arg(pathTemp);
            log->msg(s, LOG_ERROR);
        }

    }

    log->msg("    Конфигурация: ");
    log->msg("        Сервер: "+serverAddress+":"+serverPort);
    log->msg("        Клиент: "+clientAddress+":"+clientPort);
    log->msg("        Временная папка: "+pathTemp);

}

void Central::parseArgs(char *argv[]){

}

//debug function
void Central::test()
{
    QString m="";
    m=m.setNum(this->temp);

}


