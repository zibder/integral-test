#include "auth.h"

Auth::Auth()
{
    accountsFileName="accounts.dat";
    messages="";
    accounts.empty();
    accountsCnt=0;
    user.empty();
}

void Auth::init()
{
    //check file
    messages.append("    инициализиация");
    accounts.empty();
    accountsCnt=0;

    //check file
    QFile file(accountsFileName);
    if (file.open(QIODevice::ReadWrite | QIODevice::Text)){

        QString accountsData=file.readAll();
        accountsData=accountsData.replace("\t","").replace("\r","").replace("\n","");
        accountsData=accountsData.replace("\\","");

        if( !accountsData.isEmpty() ){

            //reading data
            messages.append("    аккаунты существуют, чтение\n");

            QJsonDocument accDoc;
            accDoc = QJsonDocument::fromJson(accountsData.toUtf8());

            QJsonArray list=accDoc["accounts"].toArray();
            foreach (const QJsonValue & value, list) {
                QJsonObject element = value.toObject();
                //messages=messages.append("    account login="+element["login"].toString()+" password="+element["password"].toString()+" role="+element["role"].toString()+"\n");

                QString key=element["login"].toString();
                if( !key.isEmpty() ){
                    QJsonObject account;
                    account["login"]=element["login"].toString();
                    account["password"]=element["password"].toString();
                    account["role"]=element["role"].toString();
                    accounts[key]=account;
                    accountsCnt++;
                }
            }

            messages.append("    аккаунтов"+QString::number(accountsCnt)+"\n");

        }else{

            //write deafault data
            messages.append("    аккаунты не существуют, создание");
            //FIXME:
        }

        file.close();

    }else{
        messages.append("Невозможно открыть файл аккаунтов для чтения");
    }
}

QString Auth::getMessages()
{
    return messages;
}

QJsonObject Auth::getAccounts()
{
    QJsonObject result;
    QString key="";

    if( accountsCnt>0 ){
        int i=1;
        foreach (const QJsonValue & value, accounts) {
            QJsonObject element = value.toObject();

            key=QString::number(i);
            messages=messages.append("    account id="+key+" login="+element["login"].toString()+" password="+element["password"].toString()+" role="+element["role"].toString()+"\n");

            QJsonObject account;
            account["id"]=key;
            account["login"]=element["login"].toString();
            account["password"]=element["password"].toString();
            account["role"]=element["role"].toString();
            result[key]=account;

            i++;
        }
    }

    return result;
}

void  Auth::setAccounts(QJsonObject items){

    QJsonObject accountsStruct;
    QJsonArray accountsList;
    foreach (const QJsonValue & value, items) {
        QJsonObject element = value.toObject();
        accountsList.push_front(element);
    }
    accountsStruct["accounts"]=accountsList;

    QJsonDocument accDoc(accountsStruct);
    QString accString(accDoc.toJson(QJsonDocument::Compact));

    QFile file(accountsFileName);
    if (file.open(QIODevice::ReadWrite | QIODevice::Truncate | QIODevice::Text)){

        QByteArray accByteArray=accString.toUtf8();
        file.write(accByteArray);
        file.close();

    }else{
        messages.append("Невозможно открыть файл аккаунтов для записи");
    }

}

bool Auth::checkAuth(QString login, QString password)
{
    bool result=false;

    if( accountsCnt>0 ){
        foreach (const QJsonValue & value, accounts) {
            QJsonObject element = value.toObject();
            messages=messages.append("    account login="+element["login"].toString()+" password="+element["password"].toString()+" role="+element["role"].toString()+"\n");
            if( element["login"].toString() == login ){
                if( element["password"].toString() == password ){
                    user["login"]=element["login"].toString();
                    user["role"]=element["role"].toString();
                    result=true;
                }
            }
        }
    }

    return result;
}
