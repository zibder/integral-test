//
// Created by AndreySH on 19.02.2020.
//

#pragma once

#include <QtCore>
#include <QtNetwork>

class IntegralServerOnvif : public QTcpServer
{
    Q_OBJECT
private:
    const quint16 DEFAULT_LISTEN_PORT = 18889;
    const QString CONFIG_PATH = "./application.cfg";
    const QString CONFIG__KEY__CLIENT = "client";
    const QString CONFIG__KEY__CLIENT__ONVIF_PORT = "onvifPort";

public:
    IntegralServerOnvif(qint16 port);

protected:
    void incomingConnection(qintptr handle) override;

private:
    quint16 _listenPort;

private:
    quint16 _getOnvifPortFromConfig();

public slots:
    void slotNewMessage(QHash<QString, QString> data);

signals:
    void emitNewMessage(QHash<QString, QString> data);
};
