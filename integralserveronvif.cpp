//
// Created by AndreySH on 19.02.2020.
//

#include "integralserveronvif.h"
#include "integralclienteventsonvif.h"

void IntegralServerOnvif::incomingConnection(qintptr handle)
{
    IntegralClientProcessingEventsOnvif *client = new IntegralClientProcessingEventsOnvif(handle);

    connect(client, &IntegralClientProcessingEventsOnvif::emitNewMessage, this, &IntegralServerOnvif::slotNewMessage);

    connect(client, &IntegralClientProcessingEventsOnvif::emitDisconnected, [client]() {
        client->deleteLater();
    });

    client->setSocketDescriptor(handle);
}

IntegralServerOnvif::IntegralServerOnvif(qint16 port) : _listenPort(DEFAULT_LISTEN_PORT)
{
     //listen(QHostAddress::Any, _getOnvifPortFromConfig());
    listen(QHostAddress::Any, port);
}

quint16 IntegralServerOnvif::_getOnvifPortFromConfig()
{
    QFile file(CONFIG_PATH);

    file.open(QIODevice::ReadOnly);

    if (file.error() == QFileDevice::NoError)
    {
        const QJsonObject &config = QJsonDocument::fromJson(file.readAll()).object();
        const QString &port = config[CONFIG__KEY__CLIENT].toObject()[CONFIG__KEY__CLIENT__ONVIF_PORT].toString();

        file.close();
        return port.toUInt();
    }

    file.close();

    return DEFAULT_LISTEN_PORT;
}

void IntegralServerOnvif::slotNewMessage(QHash<QString, QString> data)
{
    emitNewMessage(data);
}