#pragma once

#include <QtCore>
#include <QTimer>
#include <QTime>

#include "logger.h"
#include "auth.h"

class Central
{
public:
    //singletone struct
    Central();
    Central(int val);
    int get() const;
    void set(int val);
    static Central* instance();
    void init();
    void parseArgs(char *argv[]);

    //debug
    int temp;
    void test();

    QString serverAddress;
    QString serverPort;
    QString clientAddress;
    QString clientPort;
    QString clientOnvifAddress;
    QString clientOnvifPort;
    int debugWindow;
    int listenerTimeout;
    QString pathTemp;

    //IntegralClient *client;
    Logger *log;
    Auth *auth;
    QString buildDate;
    QString buildVersion;
    QString buildName;
    QJsonObject params;


private:
    QString configFile;



protected:
    int m_val;
    static Central* _instance;
};

