#include <QCoreApplication>
#include <QDebug>
#include "monitor.h"
#include "central.h"
#include <iostream>
#include "application.h"
#include <QCommandLineParser>

QTextStream& qStdOut();
void parseCommandLine(QCommandLineParser &parser);

int main(int argc, char *argv[])
{
    QCoreApplication app(argc, argv);


    //QCommandLineParser parser;
//        parseCommandLine(parser);

    /*
    QCommandLineParser parser;
    QCoreApplication::setApplicationName("test_users");
    parser.addHelpOption();
    QCommandLineOption targetDirectoryOption(QStringList() << "t" << "target-directory",
        QCoreApplication::translate("main", "Overwrite existing files."));
    parser.addOption(targetDirectoryOption);
    parser.process(app);

    QString targetDir = parser.value(targetDirectoryOption);

    qDebug()<<"???"<<targetDir;
    */




    setlocale(LC_ALL, "");
    qRegisterMetaType<QHash<QString, QString>>("QHash<QString, QString>");
    Application *a=new Application();
    a->bench();

    return app.exec();
}


void parseCommandLine(QCommandLineParser &parser)
{

    parser.setApplicationDescription("Explaining text");
    parser.addHelpOption();

    //An option with a value
    QCommandLineOption targetDirectoryOption(QStringList() << "t" << "target-directory",
            QCoreApplication::translate("main", "Copy all source files into <directory>."),
            QCoreApplication::translate("main", "directory"));
    parser.addOption(targetDirectoryOption);

    parser.process(QCoreApplication::arguments());

    QString target = parser.value(targetDirectoryOption);
    qDebug()<<":::"<<target;
}



