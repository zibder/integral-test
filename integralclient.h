#pragma once

#include <QObject>
#include <QtCore>
#include <QNetworkAccessManager>
#include <QUrl>
#include "request.h"
#include <QTcpServer>
#include <QTcpSocket>
#include "central.h"
#include "integralclientonvif.h"
#include "integralserveronvif.h"

class IntegralClient: public QObject
{
    Q_OBJECT
public:
    //IntegralClient();
    explicit IntegralClient(QObject *parent = nullptr );

    QString serverAddress;
    QString serverPort;
    QString clientAddress;
    QString clientPort;
    QString clientOnvifAddress;
    QString clientOnvifPort;
    qint32 listenerTimeout;

    QNetworkAccessManager *networkManager;

    Request *request;
    QString response;
    QString content;
    QString nextSegment;
    QTcpSocket* clientSocket;
    bool clientSocketCreated;

    QNetworkRequest networkRequest;
    QByteArray requestBody;
    void prepareQuery();
    void sendQuery();
    void sendQueryOnvifIntroscopyLimitExceeded();
    void sendQueryOnvifNeutronProbingDetect();

    QTcpServer *tcpServer;
    bool tcpServerStatus;
    quint16 nextBlockSize;
    void clientReadFinish(QTcpSocket*, const QString& );
    QJsonDocument parseResponse(QString& );
    QString sessionId;
    QString topic;
    QString event;
    QString processSegment();
    QString getTagValue(QString,QString);
    QString getAfterTagValue(QString,QString);
    qint64 contentLen;
    qint64 headerContentLen;

    void run();

private:
    QNetworkReply *reply;
    Central* central;
    IntegralClientOnvif* _integralClientOnvif;
    IntegralServerOnvif *_integralServerOnvif;

private:
    inline QString _serverAddrFull();
    inline QString _clientOnvifAddrFull();


signals:
    QString onSendQuery(QString&);
    void emitNewMessage(QHash<QString, QString> data);

private slots:
    void sendQueryComplete(QNetworkReply*);
    void newConnection();
    void clientRead();
    void clientRead2();

    void slotNewMessage(QHash<QString, QString> data);
};


