#pragma once

#include <QtCore>
#include <QJsonObject>
#include <QJsonArray>
#include <QJsonDocument>

class Request
{
public:
    Request();
    QString Build();

    QString clientAddress;
    QString expression;
    QString extendParams;
    QString tpl;

    QJsonObject params;
    QString body;

private:
};


