@echo off
rem server port client port requests
rem test_users.exe --users 1 --requests 10 --integral-server 10.8.0.3 --integral-port 8888 --host-ip 10.8.0.9 --host-port 18888 
echo %4 > %4.run
test_users.exe --users 1 --requests %5 --integral-server %1 --integral-port %2 --host-ip %3 --host-port %4 > %4.rlog 2>&1
del %4.run>NUL
exit