#include "integralclient.h"
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QNetworkRequest>
#include <QTextCodec>
#include <QUrl>
#include <QtNetwork>

// Monitor::Monitor(QQmlApplicationEngine *engine, QObject *parent ) : QObject(parent)
// IntegralClient::IntegralClient()
IntegralClient::IntegralClient(QObject *parent) : QObject(parent)
{
    central = Central::instance();

    // defaults
    serverAddress = "127.0.0.1";
    serverPort = "9999";
    clientAddress = "127.0.0.1";
    clientPort = "18888";
    clientOnvifPort="18889";
    tcpServerStatus = false;
    nextBlockSize = 0;

    sessionId = "";
    topic = "";
    event = "";

    response = "";

    nextSegment = "";
    content = "";
    clientSocketCreated = false;
    contentLen = 0;
    headerContentLen = 0;
    listenerTimeout = 100;

    networkManager = new QNetworkAccessManager(this);
    connect(networkManager, SIGNAL(finished(QNetworkReply *)), this, SLOT(sendQueryComplete(QNetworkReply *)));



    //onvif protocol
    /*
    qint16 onvifPort = clientOnvifPort.toInt();
    _integralClientOnvif = new IntegralClientOnvif();
    _integralServerOnvif = new IntegralServerOnvif(onvifPort);
    connect(_integralServerOnvif, &IntegralServerOnvif::emitNewMessage, this, &IntegralClient::slotNewMessage);
    */
}

void IntegralClient::run()
{

    //ext protocol
    tcpServer = new QTcpServer(this);
    connect(tcpServer, SIGNAL(newConnection()), this, SLOT(newConnection()));

    //ext protocol
    qint16 port = clientPort.toInt();
    if (!tcpServer->listen(QHostAddress::Any, port) && tcpServerStatus != true)
    {
        central->log->msg("    не удалось запустить сервер");
    }
    else
    {
        tcpServerStatus = true;
        central->log->msg("    сервер запущен " + QString::number(port));
    }

    /*
    //onvif protocol
    qint16 onvifPort = clientOnvifPort.toInt();
    _integralClientOnvif = new IntegralClientOnvif();
    _integralServerOnvif = new IntegralServerOnvif(onvifPort);
    */

    //onvif protocol

    /*
    qint16 onvifPort = clientOnvifPort.toInt();
    _integralClientOnvif = new IntegralClientOnvif();
    _integralServerOnvif = new IntegralServerOnvif(onvifPort);
    connect(_integralServerOnvif, &IntegralServerOnvif::emitNewMessage, this, &IntegralClient::slotNewMessage);

    if (_integralServerOnvif->isListening())
    {
        central->log->msg("    onvif server started port="+QString::number(onvifPort));
    }
    else
    {
        central->log->msg("    can not run onvif server port="+QString::number(onvifPort));
    }
    */

}

void IntegralClient::prepareQuery()
{
    // response="";
    // clientSocketCreated=false;

    //central->log->msg("        query");

    // client
    QString clientAddr = "http://%1:%2";
    clientAddr = clientAddr.arg(this->clientAddress).arg(this->clientPort);
    request->clientAddress = clientAddr;
    // <- http://127.0.0.1:18888

    // server
    QString serverAddr = "http://%1:%2";
    serverAddr = serverAddr.arg(serverAddress).arg(serverPort);
    QUrl requestUrl = QUrl(serverAddr);
    // -> http://127.0.0.1:9999

    central->log->msg("        " + clientAddr + "->" + serverAddr);
    central->log->msg("        topic: " + request->expression);

    if (!sessionId.isEmpty())
    {
        request->params["sessionId"] = sessionId;
    }

    QString requestText = request->Build();
    requestBody = requestText.toUtf8();
    QByteArray requestBodySize = QByteArray::number(requestBody.size());

    //central->log->msg("        params: " + request->extendParams);

    networkRequest = QNetworkRequest(requestUrl);
    networkRequest.setRawHeader("User-Agent", "Integral Monitor");
    networkRequest.setRawHeader("X-Custom-User-Agent", "Integral Monitor");

    networkRequest.setRawHeader("User-Agent", " Test/1.1");
    networkRequest.setRawHeader("Content-Type", "text/plain; charset=UTF-8");
    networkRequest.setRawHeader("Content-Length", requestBodySize);
}

void IntegralClient::sendQuery()
{
    prepareQuery();
    networkManager->post(networkRequest, requestBody);
}

void IntegralClient::sendQueryOnvifIntroscopyLimitExceeded()
{
    const QNetworkRequest &onvifActionRequest =
        _integralClientOnvif->introscopyLimitExceededSubscribeRequest(_serverAddrFull(), _clientOnvifAddrFull());
    const QByteArray &onvifActionBody = _integralClientOnvif->introscopyLimitExceededSubscribeBody(_clientOnvifAddrFull());

    networkManager->post(onvifActionRequest, onvifActionBody);
}

void IntegralClient::sendQueryOnvifNeutronProbingDetect()
{


    const QNetworkRequest &onvifActionRequest =
        _integralClientOnvif->neutronProbingDetectSubscribeRequest(_serverAddrFull(), _clientOnvifAddrFull());
    const QByteArray &onvifActionBody = _integralClientOnvif->neutronProbingDetectSubscribeBody(_clientOnvifAddrFull());

    networkManager->post(onvifActionRequest, onvifActionBody);
}

QString IntegralClient::_serverAddrFull()
{
    return QString("http://%1:%2").arg(serverAddress).arg(serverPort);
}

QString IntegralClient::_clientOnvifAddrFull()
{
    QString a=QString("http://%1:%2").arg(this->clientOnvifAddress).arg(this->clientOnvifPort);
    //central->log->msg("    back addr "+a);
    return a;
}

void IntegralClient::sendQueryComplete(QNetworkReply *reply)
{
    QVariant statusCode = reply->attribute(QNetworkRequest::HttpStatusCodeAttribute);
    central->log->msg("        response: " + statusCode.toString());
}

void IntegralClient::newConnection()
{
    // central->log->msg("        ответ сервера ->");

    // content="";
    // contentLen=0;
    // headerContentLen=0;

    clientSocket = tcpServer->nextPendingConnection();
    clientSocketCreated = true;

    // connect(clientSocket, SIGNAL(disconnected()), this, SLOT(clientDisconnect()) );
    connect(clientSocket, SIGNAL(readyRead()), this, SLOT(clientRead2()));
}

void IntegralClient::clientRead()
{
    // central->log->msg("        clientRead ");

    if (clientSocketCreated)
    {

        QString segment = "";
        qint64 b = clientSocket->bytesAvailable();
        // central->log->msg("            bytes:"+QString::number(b));

        if (b > 0)
        {
            segment = clientSocket->readAll();
            // response.append(segment);

            QString value = getTagValue(segment, "Content-Length:");
            if (!value.isEmpty())
            {
                // central->log->msg("            header");
                headerContentLen = value.toInt();
                // central->log->msg("            Content-Length:"+QString::number(contentLen));

                QString body = getAfterTagValue(segment, "\n\n");
                if (!body.isEmpty())
                {
                    content = content.append(body);
                    contentLen = contentLen + body.length();
                    // central->log->msg("            adding:"+QString::number(body.length()));
                }
            }
            else
            {
                // central->log->msg("            next");
                content = content.append(segment);
                contentLen = contentLen + segment.length();
                // central->log->msg("            adding:"+QString::number(segment.length()));
            }
        }

        // central->log->msg("            summary len:"+QString::number(contentLen)+"
        // Content-Length:"+QString::number(headerContentLen));

        if (contentLen >= headerContentLen)
        {
            // central->log->msg("            finish");
            // central->log->msg("            =="+content);

            //central->log->msg("        response processed");
            clientSocket->close();
            clientSocketCreated = false;
            response = content;
            emit onSendQuery(response);
        }
    }
}

void IntegralClient::clientRead2()
{
    central->log->msg("        clientRead2 ",LOG_TRACE2);

    QString marker = "*###*";

    if (clientSocketCreated)
    {

        QString segment = "";
        qint64 b = clientSocket->bytesAvailable();
        // central->log->msg("            Сообщение. Байт:"+QString::number(b));

        if (b > 0)
        {
            segment = clientSocket->readAll();
            central->log->msg("            seg:"+segment,LOG_TRACE2);

            if (!segment.isEmpty())
            {

                response = response.append(segment);
                // processSegment();

                QString result = "";
                bool resume = true;

                //     01234567890123456789
                //     abcde#fghi#jklmnop
                //     position=5
                //     len=9
                //     first=abcde
                //     nextSegment=fghi

                int j = 0;
                if (!response.isEmpty())
                {
                    while (resume)
                    {
                        j++;

                        int position;
                        position = response.indexOf(marker);

                        // QString msg="# position: %1";
                        // central->log->msg("            "+msg.arg(position));

                        if (position > -1)
                        {
                            result = response.mid(0, position);
                            response = response.mid(position + marker.length(), (response.length() - position));
                            resume = true;
                        }
                        else
                        {
                            // result=response;
                            // response="";
                            resume = false;
                        }

                        if (resume)
                        {
                            if (!result.isEmpty())
                            {
                                // central->log->msg("            Сообщение прочитано");
                                emit onSendQuery(result);
                            }
                        }
                    }
                }
            }
        }
    }
}

QString IntegralClient::processSegment()
{
    QString result = "";
    bool resume = true;

    //     01234567890123456789
    //     abcde#fghi#jklmnop
    //     position=5
    //     len=9
    //     first=abcde
    //     nextSegment=fghi

    int j = 0;
    if (!response.isEmpty())
    {
        while (resume)
        {
            j++;

            int position;
            position = response.indexOf("#");
            if (position > -1)
            {
                result = response.mid(0, position);
                response = response.mid(position, (response.length() - position));
                resume = true;
            }
            else
            {
                result = response;
                response = "";
                resume = false;
            }

            if (!result.isEmpty())
            {
                central->log->msg("        response processed");
                central->log->msg("        ::" + result);
                // emit onSendQuery(result);
            }

            if (j > 10)
            {
                resume = false;
            }
        }
    }
}

QString IntegralClient::getTagValue(QString content, QString tag)
{
    QString result = "";

    if (!content.isEmpty() && !tag.isEmpty())
    {

        int startPos = 0;
        int finishPos = 0;
        QString val = "";

        startPos = content.indexOf(tag);
        if (startPos > -1)
        {
            startPos = startPos + tag.length();
            finishPos = content.indexOf("\n", startPos + 1);
            if (finishPos > -1)
            {
                val = content.mid(startPos, (finishPos - startPos));
            }
        }

        if (!val.isEmpty())
        {
            result = val;
            result = result.trimmed();
        }
    }

    return result;
}

QString IntegralClient::getAfterTagValue(QString content, QString tag)
{
    QString result = "";

    if (!content.isEmpty() && !tag.isEmpty())
    {

        int startPos = 0;
        int finishPos = 0;
        QString val = "";

        startPos = content.indexOf(tag);
        if (startPos > -1)
        {
            startPos = startPos + tag.length();
            finishPos = content.length();
            if (finishPos > -1)
            {
                val = content.mid(startPos, (finishPos - startPos));
            }
        }
        else
        {
            val = "";
        }

        if (!val.isEmpty())
        {
            result = val;
        }
    }

    return result;
}

QJsonDocument IntegralClient::parseResponse(QString &response)
{
    QJsonDocument result;
    event = "";

    if (!response.isEmpty())
    {

        QStringList list;
        // list=response.split("\r\n\r\n");
        // QString jsonString=list.at(1);

        if (!response.isEmpty())
        {

            QByteArray jsonArray = response.toUtf8();
            QJsonDocument r = QJsonDocument().fromJson(jsonArray);

            /*
            if( !r["sessionId"].toString().isEmpty() ){
                //sessionId=r["sessionId"].toString().replace("{","").replace("}","");
                sessionId=r["sessionId"].toString();
            }

            if( !r["topic"].toString().isEmpty() ){
                topic=r["topic"].toString();
            }
            */

            if (!r["event"].toString().isEmpty())
            {
                event = r["event"].toString().toLower();
            }

            // central->log->msg("        событие: "+sessionId);

            result = r;
        }
    }

    return result;
}

void IntegralClient::slotNewMessage(QHash<QString, QString> data)
{
    emitNewMessage(data);
}
