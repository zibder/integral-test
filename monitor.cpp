#include "monitor.h"

#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonObject>
#include <QObject>
#include <QRandomGenerator>
#include <QVariant>

// Monitor::Monitor(QQmlApplicationEngine *engine, QObject *parent ) : QObject(parent)
Monitor::Monitor(QObject *parent) : QObject(parent)
{
    // this->engine=engine;
    // root = engine->rootObjects()[0];

    central = Central::instance();

    // QObject::connect(this->parent(), SIGNAL(testSignal(QString)), this, SLOT(onTestSignal(QString)));
    // QObject::connect(this->parent(), SIGNAL(testUpdDevices(QString)), this, SLOT(onTestUpdDevices(QString)));
    //QObject::connect(this->parent(), SIGNAL(showDbgWindowSignal()), this, SLOT(showDbgWindowSlot()));
    //QObject::connect(this->parent(), SIGNAL(updateDevicesSignal()), this, SLOT(updateDevicesSlot()));
    //QObject::connect(this->parent(), SIGNAL(updateMessagesSignal()), this, SLOT(updateMessagesSlot()));
    //QObject::connect(this->parent(), SIGNAL(archiveModeOnSignal()), this, SLOT(archiveModeOnSlot()));
    //QObject::connect(this->parent(), SIGNAL(archiveModeOffSignal()), this, SLOT(archiveModeOffSlot()));
    //QObject::connect(this->parent(), SIGNAL(getArchiveSignal(QString, QString)), this, SLOT(getArchiveSlot(QString, QString)));
    //QObject::connect(this->parent(), SIGNAL(getImageSignal(QString, QString)), this, SLOT(getImageSlot(QString, QString)));
    //QObject::connect(this->parent(), SIGNAL(showUsersWindowSignal()), this, SLOT(showUsersWindowSlot()));

    // timers

    // DEPRECATED:
    deviceTimer = new QTimer();
    connect(deviceTimer, SIGNAL(timeout()), this, SLOT(deviceTimerSlot()));

    // DEPRECATED:
    anonceTimer = new QTimer();
    connect(anonceTimer, SIGNAL(timeout()), this, SLOT(anonceTimerSlot()));

    initTimer = new QTimer();
    connect(initTimer, SIGNAL(timeout()), this, SLOT(initTimerSlot()));

    renewTimer = new QTimer();
    connect(renewTimer, SIGNAL(timeout()), this, SLOT(renewTimerSlot()));

    waitResponceTimer = new QTimer();
    connect(waitResponceTimer, SIGNAL(timeout()), this, SLOT(waitResponceTimerSlot()));

    // creating client
    client = new IntegralClient(this);

    connect(client, SIGNAL(onSendQuery(QString &)), this, SLOT(processResponse(QString &)));
    connect(client, &IntegralClient::emitNewMessage, this, &Monitor::slotNewMessage);

    // delay for qml rendering

    /*
    if(!central->debugWindow){

        //creating client
        client=new IntegralClient(this);
        connect(client, SIGNAL(onSendQuery(QString&)), this, SLOT(processResponse(QString&)));

        //delay for qml rendering
        initTimer->start(3000);

    }else{
        showDbgWindowSlot();
    }
    */

    sessionInitialized = false;
    devicesCnt = 0;
    messagesCnt = 0;
    currentDeviceIndex = 0;
    currentMessageIndex = 0;
    waitingResponce = false;
    currentQuery = 0;
    firstFindingDevices = false;
    parseMessagesRealtime = true;
    archiveDateFrom = "";
    archiveDateTo = "";
    archiveImageSerialNumber = "";
    archiveImageFileName = "";
    clientPort = "";
    clientOnvifPort="";

    stateConnected = false;
    stateMessages = 0;
    stateDevices = 0;

    //completed
    queriesComplete=0;
    queriesError=0;
    //current queries
    queriesCnt=0;
    //max queries todo
    queriesMax=0;
    //testing complete
    complete=false;
    name="";

    req=0;
    ret=0;

    parameters.empty();
    parameters["show_debug_buttons"] = "0";
    parameters["show_users_button"] = "0";

    if (central->auth->user["role"] == "admin")
    {
        parameters["show_users_button"] = "1";
    }

    // showLogWindowSlot();
}

void Monitor::run()
{

    if (!this->clientPort.isEmpty())
    {
        client->clientPort = this->clientPort;
    }
    if (!this->clientOnvifPort.isEmpty())
    {
        client->clientOnvifPort = this->clientOnvifPort;
    }
    client->run();

    initTimer->start(100);
}

void Monitor::showDbgWindowSlot()
{
}

void Monitor::updateDevicesSlot()
{
    // qDebug() << "SLOT: updateDevicesSlot: ";
    testUpdateDevices();
}

void Monitor::updateMessagesSlot()
{
    // qDebug() << "SLOT: updateMessagesSlot: ";
    testUpdateMessages();
}

void Monitor::archiveModeOnSlot()
{
    qDebug() << "SLOT: archiveModeOnSlot: ";

    parseMessagesRealtime = false;
    if (devicesCnt > 0 && messagesCnt>0 ){
        messagesCnt = 0;
        emit clearMessagesSignal();
    }

}

void Monitor::archiveModeOffSlot()
{
    qDebug() << "SLOT: archiveModeOffSlot: ";
    parseMessagesRealtime = true;
    messagesCnt = 0;
    emit clearMessagesSignal();
}

void Monitor::getArchiveSlot(const QString &dateFrom, const QString &dateTo)
{
    qDebug() << "SLOT: getArchiveSlot: dateFrom=" + dateFrom + " dateTo=" + dateTo;
    // dateFrom="09.02.2020"

    archiveDateFrom = "";
    if (!dateFrom.isEmpty())
    {
        QDate d = QDate::fromString(dateFrom, "dd.MM.yyyy");
        archiveDateFrom = d.toString("yyyy-MM-dd");
    }

    archiveDateTo = "";
    if (!dateTo.isEmpty())
    {
        QDate d = QDate::fromString(dateTo, "dd.MM.yyyy");
        archiveDateTo = d.toString("yyyy-MM-dd");
    }

    // sendQuery2(QUERY2_GET_ARCHIVE);
    sendQuery2(QUERY2_GET_ARCHIVE_FILTERED);
}

void Monitor::getImageSlot(const QString &sn, const QString &fn)
{
    qDebug() << "SLOT: getImageSlot: sn=" + sn + " fn=" + fn;
    // id=2476

    if (!sn.isEmpty() && !fn.isEmpty())
    {
        archiveImageSerialNumber = sn;
        archiveImageFileName = fn;
        sendQuery2(QUERY2_GET_ARCHIVE_IMAGE);
    }
}

void Monitor::setParameters()
{
    //qDebug() << "setParameters: ";
    QJsonDocument jdoc(parameters);
    QString jstr(jdoc.toJson(QJsonDocument::Compact));
    emit setParametersSignal(jstr);
}

void Monitor::showUsersWindowSlot()
{
}

void Monitor::showLogWindowSlot()
{
}

void Monitor::initTimerSlot()
{
    // qDebug() << "SLOT: initTimerSlot: ";

    initTimer->stop();
    setParameters();
    mainLoop();

    // sendQuery(QUERY_GET_SESSION);
    // nextQuery();

    /*
    //attempting to get session
    sendQuery(QUERY_GET_SESSION);
    //->runGetDevices

    //repeating to get session
    initTimer->stop();
    initTimer->start(10000);
    */
}

void Monitor::renewTimerSlot()
{
    // qDebug() << "SLOT: renewTimerSlot: ";
    sendQuery2(QUERY2_RENEW);
}

void Monitor::runGetDevices()
{
    initTimer->stop();

    // qDebug() << "SLOT: runGetDevices: ";
    sendQuery(QUERY_GET_DEVICES);
    //->runWorkingTimers

    deviceTimer->stop();
    deviceTimer->start(10000);
}

void Monitor::runWorkingTimers()
{
    // qDebug() << "SLOT: runWorkingTimers: ";

    anonceTimerSlot();

    // starting timers
    deviceTimer->stop();
    deviceTimer->start(30000);

    // anonceTimer->start(3000);
}

void Monitor::anonceTimerSlot()
{
    // qDebug() << "SLOT: anonceTimerSlot: ";
    // testUpdateMessages();
    sendQuery(QUERY_GET_MESSAGES);
}

void Monitor::deviceTimerSlot()
{
    // qDebug() << "SLOT: deviceTimerSlot: ";
    // testUpdateDevices();
    sendQuery(QUERY_GET_DEVICES);
}

void Monitor::testUpdateDevices()
{
    // qDebug() << "testUpdateDevices ";

    QJsonObject device;
    QString k;

    devices.empty();

    k = "1";
    device["name"] = k;
    device["groupName"] = "DEVICE_NEUTRON_PROBING_KERBER";
    device["groupId"] = "1";
    device["ip"] = "192.168.1.1";
    device["serialNumber"] = "001-00001";
    device["status"] = "1";
    device["typeId"] = "7";
    device["typeCaption"] = "Поиск взрывчатых веществ";
    device["deviceCaption"] = "Кербер";
    devices[k] = device;
    devicesCnt++;

    k = "2";
    device["name"] = k;
    device["groupName"] = "DEVICE_NEUTRON_PROBING_CHEMICAL_EXPERT";
    device["groupId"] = "2";
    device["ip"] = "192.168.1.2";
    device["serialNumber"] = "002-00001";
    device["status"] = "1";
    device["typeId"] = "7";
    device["typeCaption"] = "Поиск взрывчатых веществ";
    device["deviceCaption"] = "Химэксперт";
    devices[k] = device;
    devicesCnt++;

    k = "3";
    device["name"] = k;
    device["groupName"] = "DEVICE_INTROSCOPY_RAPISCAN_60XX";
    device["groupId"] = "3";
    device["ip"] = "192.168.1.3";
    device["serialNumber"] = "003-00001";
    device["status"] = "0";
    device["typeId"] = "4";
    device["typeCaption"] = "Интроскопия";
    device["deviceCaption"] = "Рапискан";
    devices[k] = device;
    devicesCnt++;

    QJsonDocument jdoc(devices);
    QString jstr(jdoc.toJson(QJsonDocument::Compact));
    emit updateDevicesSignal(jstr);
}

void Monitor::testUpdateMessages()
{
    QJsonObject messages;
    QJsonObject message;
    QString k;

    // devices.empty();

    /*
    k = QString::number( (int)QRandomGenerator::global()->generate() );
    message["name"]=k;
    message["serialNumber"]="003-00001";
    message["eventName"]="detect";
    message["eventCaption"]="обнаружение";
    message["dateTime"]="08.02.2020 23:15:49";
    message["message"]="Подозрительное содержимое, концентрация 0.8 ПДК, вероятность обнаружения 80%";
    message["device"]=devices["3"];
    messages[k]=message;

    k = QString::number( (int)QRandomGenerator::global()->generate() );
    message["name"]=k;
    message["serialNumber"]="001-00001";
    message["eventName"]="detect";
    message["eventCaption"]="обнаружение";
    message["dateTime"]="08.02.2020 23:15:49";
    message["message"]="Возможно, взрывоопасно, этанол, концентрация 0.8 ПДК, вероятность обнаружения 90%";
    message["device"]=devices["1"];
    messages[k]=message;
    */

    bool deviceAssigned = false;
    QJsonObject device;

    if (devicesCnt > 0)
    {
        QJsonObject devicesList = devices;
        foreach (const QJsonValue &value, devicesList)
        {

            QJsonObject elementDevice = value.toObject();

            if (!elementDevice["serialNumber"].toString().isEmpty())
            {
                // if( elementDevice["serialNumber"].toString() == serialNumber ){
                if (!deviceAssigned)
                {
                    deviceAssigned = true;
                    device = elementDevice;
                }
            }
        }
    }

    k = QString::number(messagesCnt);
    message["name"] = k;
    message["serialNumber"] = "3452345";
    message["eventName"] = "detect";
    message["eventCaption"] = "обнаружение";
    message["dateTime"] = "2020-02-16 00:00:00";
    message["message"] = "message";
    message["message0"] = "anonce";
    message["fileName"] = "";
    message["anonce"] = "anonce";
    message["device"] = device;
    messages[k] = message;
    messagesCnt++;

    QJsonDocument jdoc(messages);
    QString jstr(jdoc.toJson(QJsonDocument::Compact));
    emit updateMessagesSignal(jstr);

    /*
    QJsonObject message;
    QJsonObject messages;
    QString k="";
    qint16 serial;
    qint32 n=(qrand() % ((5 + 1) - 1) + 1);

    serial = (int)QRandomGenerator::global()->generate();
    k=QString(serial);
    message["name"]=k;
    message["type"]="Уведомление";
    message["ondate"]="20 СЕН 2019 22:55:12";
    message["message"]="Задымление в контрольной зоне ";
    message["deviceTypeId"]=3;
    message["deviceTitle"]="Пожарная сигнализация";
    message["deviceSerial"]=serial;
    message["deviceModel"]="ПП969";
    messages[k]=message;

    serial = (int)QRandomGenerator::global()->generate();
    k=QString(serial);
    message["name"]=k;
    message["type"]="Уведомление";
    message["ondate"]="20 СЕН 2019 22:55:12";
    message["message"]="Металличесике предметы большого объема";
    message["deviceTypeId"]=4;
    message["deviceTitle"]="Интроскопия";
    message["deviceSerial"]=serial;
    message["deviceModel"]="ПП969";
    messages[k]=message;

    serial = (int)QRandomGenerator::global()->generate();
    k=QString(serial);
    message["name"]=k;
    message["type"]="Уведомление";
    message["ondate"]="20 СЕН 2019 22:55:12";
    message["message"]="Обнаружен метан";
    message["deviceTypeId"]=5;
    message["deviceTitle"]="Газовый анализ";
    message["deviceSerial"]=serial;
    message["deviceModel"]="ПП969";
    messages[k]=message;

    serial = (int)QRandomGenerator::global()->generate();
    k=QString(serial);
    message["name"]=k;
    message["type"]="Уведомление";
    message["ondate"]="20 СЕН 2019 22:55:12";
    message["message"]="Превышение радиационного фона";
    message["deviceTypeId"]=6;
    message["deviceTitle"]="Радиационный контроль";
    message["deviceSerial"]=serial;
    message["deviceModel"]="ПП969";
    messages[k]=message;

    serial = (int)QRandomGenerator::global()->generate();
    k=QString(serial);
    message["name"]=k;
    message["type"]="Уведомление";
    message["ondate"]="20 СЕН 2019 22:55:12";
    message["message"]="Опасность, тол/тротил";
    message["deviceTypeId"]=7;
    message["deviceTitle"]="Обнаружение взрывчатых веществ";
    message["deviceSerial"]=serial;
    message["deviceModel"]="ПП969";
    messages[k]=message;
    */
}

void Monitor::setClientParams()
{
    // set client params
    client->serverAddress = central->serverAddress;
    client->serverPort = central->serverPort;
    client->clientAddress = central->clientAddress;
    client->clientPort = central->clientPort;
    client->listenerTimeout = central->listenerTimeout;
    client->clientOnvifAddress = central->clientOnvifAddress;
    client->clientOnvifPort = central->clientOnvifPort;

    if (!this->clientPort.isEmpty())
    {
        client->clientPort = this->clientPort;
    }

    if (!this->clientOnvifPort.isEmpty())
    {
        client->clientOnvifPort = this->clientOnvifPort;
    }

    Request *request = new Request();
    client->request = request;
}

void Monitor::mainLoop()
{
    // central->log->msg("Main Loop. Query:"+QString::number(currentQuery) );

    //central->log->msg("        mainLoop client:"+name+" q:"+QString::number(currentQuery),LOG_TRACE);

    /*
        currentQuery = 0 at first

        QUERY2_INIT            = 0;
        QUERY2_GET_SETTINGS    = 1;
        QUERY2_SUBSCRIBE       = 2;
        QUERY2_RENEW           = 3;
        QUERY2_UNSUBSCRIBE     = 4;
        QUERY2_GET_ARCHIVE     = 5;
     */

    switch (currentQuery)
    {

        case QUERY2_INIT:
            // FIXME: repeat if failed
            sendQuery2(QUERY2_INIT);
            currentQuery = QUERY2_GET_SETTINGS;

            break;

        case QUERY2_GET_SETTINGS:

            if( !complete && !waitingResponce){
                sendQuery2(QUERY2_GET_SETTINGS);
                currentQuery = QUERY2_GET_SETTINGS;
            }


            /*
            if( queriesCnt < queriesMax ){
                sendQuery2(QUERY2_GET_SETTINGS);
                currentQuery = QUERY2_GET_SETTINGS;

            }else{
                complete=true;
                currentQuery = QUERY2_END;
            }
            */


            break;

        /*
        case QUERY2_SUBSCRIBE:
            sendQuery2(QUERY2_SUBSCRIBE);

            currentQuery = QUERY2_END;
            break;

        case QUERY2_GET_ARCHIVE:
            sendQuery2(QUERY2_GET_ARCHIVE);
            currentQuery = QUERY2_END;
            break;
        */
    }
}

void Monitor::sendQuery2(int query)
{
    //central->log->msg("query: " + QString::number(query));

    QString mark1= QString::number(QDateTime::currentMSecsSinceEpoch());
    qint32 n = (qrand() % ((9000000 + 1000000) - 1000000) + 1000000);
    QString mark2 = QString::number(n);
    QString mark = mark1+mark2;

    central->log->msg("    request  client:"+name+" q:"+QString::number(query)+" mark:"+mark+" N:"+QString::number(req)+" port:"+clientPort,LOG_TRACE);

    if( waitingResponce ){
        return;
    }

    waitingResponce = true;
    waitResponceTimer->start(30000);

    QString msg = "";

    QJsonArray mySerialNumbers = {};
    QJsonArray myEvents = {};



    switch (query)
    {

        case QUERY2_INIT:

            msg = "connecting to server";
            msg = msg.append(" ");
            msg = msg.append(central->serverAddress);
            msg = msg.append(":");
            msg = msg.append(central->serverPort);
            //logMessage(msg);

            setClientParams();
            client->request->expression = TOPIC2_INIT;
            client->request->params = {};
            client->sendQuery();
            break;

        case QUERY2_GET_SETTINGS:

            msg = "finding devices";
            //logMessage(msg);

            req++;
            setClientParams();
            client->request->expression = TOPIC2_GET_SETTINGS;
            client->request->params = {};
            client->request->params["marker"]=mark;

            client->sendQuery();
            break;


        case QUERY2_SUBSCRIBE:
            break;
            //client->sendQueryOnvifNeutronProbingDetect();
            //client->sendQueryOnvifIntroscopyLimitExceeded();

            // FIXME:
            /*
            msg="Подписка на события";
            //logMessage(msg);
            central->log->msg(msg);

            setClientParams();
            client->request->expression=TOPIC2_SUBSCRIBE;
            client->request->params={};


            mySerialNumbers.push_front("001-00002");
            mySerialNumbers.push_front("001-00003");
            client->request->params["serialNumbers"]=mySerialNumbers;

            myEvents.push_front("tns:Device/NeutronProbing/Detect");
            client->request->params["events"]=myEvents;

            //FIXME: temporary disabled
            client->sendQuery();
            central->log->msg(client->request->body);
            */
            break;

        case QUERY2_GET_ARCHIVE:
        break;

            msg = "get archive";
            // logMessage(msg);
            //central->log->msg(msg);

            setClientParams();
            client->request->expression = TOPIC2_GET_ARCHIVE;
            client->request->params = {};
            client->sendQuery();
            break;

        case QUERY2_GET_ARCHIVE_FILTERED:
        break;

            msg = "get archive by date";
            // logMessage(msg);
            //central->log->msg(msg);

            setClientParams();
            client->request->expression = TOPIC2_GET_ARCHIVE_FILTERED;
            client->request->params = {};
            client->request->params["dateFrom"] = archiveDateFrom;
            client->request->params["dateTo"] = archiveDateTo;
            client->sendQuery();
            break;

        case QUERY2_GET_ARCHIVE_IMAGE:
        break;

            msg = "Получение изображения";
            // logMessage(msg);
            //central->log->msg(msg);

            setClientParams();
            client->request->expression = TOPIC2_GET_ARCHIVE_IMAGE;
            client->request->params = {};
            client->request->params["serialNumber"] = archiveImageSerialNumber;
            client->request->params["fileName"] = archiveImageFileName;

            client->sendQuery();
            break;

        case QUERY2_RENEW:
        break;

            msg = "Продление подписки";
            // logMessage(msg);
            //central->log->msg(msg);

            setClientParams();
            client->request->expression = TOPIC2_RENEW;
            client->request->params = {};
            client->sendQuery();
            break;
    }
}

void Monitor::waitResponceTimerSlot()
{
    central->log->msg("    client:"+name+" TIMEOUT",LOG_TRACE);
    queriesError++;
    waitResponceTimer->stop();
    waitingResponce = false;

    mainLoop();

}

void Monitor::processResponse(QString &response)
{
    // central->log->msg("Ответ:");

    //if( !waitingResponce ){
    //    return;
    //}

    waitResponceTimer->stop();
    waitingResponce = false;

    QJsonDocument r = client->parseResponse(response);

    /*
    central->log->msg("Ответ. event:"+client->event);
    central->log->msg("---vvv--------------------------");
    central->log->msg(response);
    central->log->msg("---^^^--------------------------");
    */

    bool processed = false;

    if (client->event == "tns1:device/virtualchip/init")
    {
        central->log->msg("event:" + client->event);
        /*
            {"terminationTime":"2020-02-09T19:42:08"}
        */
        QJsonArray list = r["message"].toArray();
        foreach (const QJsonValue &value, list)
        {
            QJsonObject element = value.toObject();
            if (!element["terminationTime"].toString().isEmpty())
            {
                central->log->msg("    terminationTime:" + element["terminationTime"].toString());
                // FIXME: calculate me
                //renewTimer->start(30000);
                processed = true;

            }
        }

        processed = true;
        central->log->msg("    ok");

        if( !stateConnected ){
            stateConnected = true;
            mainLoop();
        }

    }

    if (client->event == "tns1:device/virtualchip/getsettings")
    {
        central->log->msg("event:" + client->event);
        // central->log->msg(response);
        /*
            {
                "deviceGroupId":1,
                "deviceGroupName":"DEVICE_NEUTRON_PROBING__KERBER",
                "ip":"127.0.0.1",
                "serialNumber":"001-00002",
                "status":true
            },
            {
                "deviceGroupId":1,
                "deviceGroupName":"DEVICE_NEUTRON_PROBING__KERBER",
                "ip":"127.0.0.1",
                "serialNumber":"001-00003",
                "status":true
            }
            {
                "deviceGroupId":3,
                "deviceGroupName":"DEVICE_INTROSCOPY__RAPISCAN_60XX",
                "ip":"10.8.0.3",
                "serialNumber":"003-00001",
                "status":true
            }

            {"event":"tns1:device/virtualchip/getsettings","message":[
            {"deviceGroupId":1,
            "deviceGroupName":"DEVICE_NEUTRON_PROBING__KERBER",
            "ip":"10.8.0.3","serialNumber":"001-00005","status":true},
            {"deviceGroupId":2,
            "deviceGroupName":"DEVICE_NEUTRON_PROBING__CHEMICAL_EXPERT",
            "ip":"192.168.11.248","serialNumber":"002-00005","status":true},
            {"deviceGroupId":3,"deviceGroupName":"DEVICE_INTROSCOPY__RAPISCAN_60XX",
            "ip":"192.168.11.248","serialNumber":"003-00005","status":true}
            ],"messageAdvance":[]}
        */

        //if (devicesCnt == 0)
        //{

            QString mark="";
            QJsonObject device;
            QString k;

            devices.empty();
            devicesCnt = 0;

            QJsonArray list = r["message"].toArray();
            foreach (const QJsonValue &value, list)
            {

                QJsonObject element = value.toObject();

                mark=element["marker"].toString();

                int status = 0;
                if (element["status"].toBool())
                {
                    status = 1;
                }
                status = 0;

                int typeId = 0;
                int vendorId = 0;
                QString typeCaption = "";
                QString deviceCaption = "";

                /*
                    {
                        "deviceGroupId":1,
                        "deviceGroupName":"DEVICE_NEUTRON_PROBING__KERBER",
                        "ip":"127.0.0.1",
                        "serialNumber":"001-00003",
                        "status":true
                    }
                */
                if (element["deviceGroupName"].toString().toLower().indexOf("neutron_probing") > -1)
                {
                    typeId = DEVICE2_TYPE_NEUTRON_PROBING;
                    typeCaption = "Обнаружение взрывчатых веществ";
                }

                if (element["deviceGroupName"].toString().toLower().indexOf("kerber") > -1)
                {
                    vendorId = DEVICE2_VENDOR_KERBER;
                    deviceCaption = "Кербер";
                }

                if (element["deviceGroupName"].toString().toLower().indexOf("chemical_expert") > -1)
                {
                    vendorId = DEVICE2_VENDOR_CHEMICAL_EXPERT;
                    deviceCaption = "Химэксперт";
                }

                /*
                    {
                        "deviceGroupId":3,
                        "deviceGroupName":"DEVICE_INTROSCOPY__RAPISCAN_60XX",
                        "ip":"10.8.0.3",
                        "serialNumber":"003-00001",
                        "status":true
                    }
                */
                if (element["deviceGroupName"].toString().toLower().indexOf("introscopy") > -1)
                {
                    typeId = DEVICE2_TYPE_INTROSCOPY;
                    typeCaption = "Интроскопия";
                }

                if (element["deviceGroupName"].toString().toLower().indexOf("rapiscan") > -1)
                {
                    vendorId = DEVICE2_VENDOR_RAPISCAN;
                    deviceCaption = "Рапискан";
                }

                k = element["serialNumber"].toString();
                device["name"] = k;
                device["groupName"] = element["deviceGroupName"].toString();
                device["groupId"] = element["deviceGroupId"].toString();
                device["ip"] = element["ip"].toString();
                device["serialNumber"] = element["serialNumber"].toString();
                device["status"] = status;
                device["typeId"] = typeId;
                device["vendorId"] = vendorId;
                device["typeCaption"] = typeCaption;
                device["deviceCaption"] = deviceCaption;
                devices[k] = device;
                devicesCnt++;

                QString msg = "";
                msg = msg.append(deviceCaption);
                msg = msg.append(" ");
                msg = msg.append(element["serialNumber"].toString());
                central->log->msg(QString("        ").append(msg));
            }

            QJsonDocument jdoc(devices);
            QString jstr(jdoc.toJson(QJsonDocument::Compact));
            emit updateDevicesSignal(jstr);

            QString msg = "";
            msg = "devices: %1";
            msg = msg.arg(devicesCnt);
            logMessage(msg);

            processed = true;
            stateDevices = devicesCnt;
            central->log->msg(QString("    ok ").append(msg));

            queriesCnt++;

            ret++;

            QString c="";
            if( queriesCnt < queriesMax ){
                mainLoop();
                //initTimer->start(100);
            }else{
                complete=true;
                c=" COMPLETE";
            }


            central->log->msg("    response client:"+name+" query:GET_SETTINGS"+" mark:"+mark+" N:"+QString::number(ret)+" qs:"+QString::number(queriesCnt)+"/"+QString::number(queriesMax)+" er:"+QString::number(queriesError)+" port:"+clientPort+" "+c,LOG_TRACE);


        //}
        //else
        //{
        //    processed = true;
        //    central->log->msg(QString("    ignore"));
        //}
    }

    if (client->event == "tns1:device/virtualchip/subscribe")
    {
        central->log->msg("event:" + client->event);
        central->log->msg(response);
        /*

        */
        // FIXME: implement me
    }

    if (client->event == "tns1:device/virtualchip/status")
    {
        central->log->msg("event:" + client->event);
        // central->log->msg(response);
        /*
            {"id":"001-00002","status":1,"utcTime":"2020-02-10T20:46:17"}
        */

        // devices.empty();

        QString serialNumber = "";
        int status = 0;

        QJsonArray list = r["message"].toArray();
        foreach (const QJsonValue &value, list)
        {

            QJsonObject element = value.toObject();
            if (!element["id"].toString().isEmpty())
            {
                serialNumber = element["id"].toString();
                status = element["status"].toInt();
            }
        }

        if (!serialNumber.isEmpty())
        {
            status = 1;
            emit updateDeviceStateSignal(serialNumber, status);
        }

        processed = true;
        central->log->msg("    ok");
    }


    if (false && parseMessagesRealtime)
    {

        if (client->event == "tns1:device/neutronprobing/detect")
        {
            central->log->msg("Событие:" + client->event);
            // central->log->msg(response);

            if (devicesCnt > 0)
            {

                QJsonObject messages = parseMessages(r, false);

                QJsonDocument jdoc(messages);
                QString jstr(jdoc.toJson(QJsonDocument::Compact));
                emit updateMessagesSignal(jstr);

                processed = true;
                central->log->msg("    ok");
            }
            else
            {
                processed = true;
                central->log->msg("    игнор (нет устройств)");
            }
        }

        if (client->event == "tns1:device/introscopy/limitexceeded")
        {
            central->log->msg("Событие:" + client->event);
            // central->log->msg(response);
            /*
                {
                    "event":"tns1:device/introscopy/limitexceeded",
                    "message":[],"messageAdvance":[{
                        "fileName":"./ManualArchives/12345_20191018145153468_SCREENER.PNG",
                        "id":"003-00001"}
                    ]}
             */

            if (devicesCnt > 0)
            {

                QJsonObject messages = parseMessages(r, false);

                QJsonDocument jdoc(messages);
                QString jstr(jdoc.toJson(QJsonDocument::Compact));
                emit updateMessagesSignal(jstr);

                processed = true;
                central->log->msg("    ok");
            }
            else
            {
                processed = true;
                central->log->msg("    игнор (нет устройств)");
            }
        }
    }
    else
    {

        if (client->event == "tns1:device/neutronprobing/detect")
        {
            processed = true;
        }
        if (client->event == "tns1:device/introscopy/limitexceeded")
        {
            processed = true;
        }
    }

    if (client->event == "tns1:device/virtualchip/getarchive" || client->event == "tns1:device/virtualchip/getarchivefiltered")
    {
        central->log->msg("Событие:" + client->event);
        // central->log->msg(response);

        //central->log->msg("данные архива получены, разбор");

        if (devicesCnt > 0)
        {

            QJsonObject messages = parseMessages(r, true);
            // dumpMessages(messages);

            QJsonDocument jdoc(messages);
            QString jstr(jdoc.toJson(QJsonDocument::Compact));
            emit updateMessagesSignal(jstr);

            QString cnt= QString::number( messages.count() );
            central->log->msg("разбор завершен, сообщений="+cnt);

            processed = true;
            central->log->msg("    ok");
        }
        else
        {
            processed = true;
            central->log->msg("    игнор (нет устройств)");
        }
    }

    if (client->event == "tns1:device/virtualchip/getarchiveimage")
    {
        central->log->msg("Событие:" + client->event);
        // central->log->msg(response);

        QJsonArray list = r["messageAdvance"].toArray();
        foreach (const QJsonValue &value, list)
        {

            QJsonObject element = value.toObject();

            QString imgContent = element["content"].toString();
            if (!imgContent.isEmpty())
            {

                QByteArray baimg = QByteArray::fromBase64(imgContent.toLatin1());

                QTemporaryDir dir;
                if (dir.isValid())
                {

                    // "C:/Users/zibder/AppData/Local/Temp/monitor-IxaBnL"
                    // QString fileName=dir.path();
                    QString fileName = "";
                    fileName = fileName.append(central->pathTemp);

                    qint32 n = (qrand() % ((900000 + 100000) - 100000) + 100000);
                    QString s = QString::number(n);
                    fileName = fileName.append(s);

                    QFile file(fileName);
                    if (!file.open(QIODevice::WriteOnly))
                    {
                        // qDebug() << "ERROR: Can not write to file: ["<< fileName<< "]";
                    }
                    else
                    {
                        file.setPermissions(QFileDevice::ReadOwner | QFileDevice::WriteOwner);
                        file.write(baimg);
                        file.close();
                    }

                    // qDebug() << "Image stored to file: ["<< fileName<< "]";
                    central->log->msg("    изображение сохранено:" + fileName);

                    emit showImageSignal(fileName);
                }
            }
        }

        processed = true;
        central->log->msg("    ok");
    }

    if (client->event == "tns1:device/virtualchip/renew")
    {
        central->log->msg("Событие:" + client->event);
        // central->log->msg(response);
        /*
            {"terminationTime":"2020-02-09T19:42:08"}
        */
        QJsonArray list = r["message"].toArray();
        foreach (const QJsonValue &value, list)
        {
            QJsonObject element = value.toObject();
            if (!element["terminationTime"].toString().isEmpty())
            {
                central->log->msg("    terminationTime:" + element["terminationTime"].toString());
                // FIXME: calculate me
                renewTimer->start(30000);
                processed = true;
            }
        }

        processed = true;
        central->log->msg("    ok");
    }

    if (!processed)
    {
        central->log->msg("Событие:" + client->event);
        central->log->msg("    необработанное");
        central->log->msg(response);
    }
}

void Monitor::processResponseXml(QHash<QString, QString> data){

    QJsonObject messages;
    QJsonObject message;
    QString k;
    bool messageProcessed=false;

    if (!parseMessagesRealtime){
        return;
    }

    if( devicesCnt==0 ){
        return;
    }

    if (!data.isEmpty())
    {
       if( !QString(data["Topic"]).isEmpty() ){

           QString topic=QString(data["Topic"]).toLower();
           QJsonObject device;
           if( !QString(data["Id"]).isEmpty() ){
               device=getDeviceBySerialNumber(data["Id"]);
           }

           central->log->msg("Message topic="+topic + " sn="+data["Id"]);

           QString eventCaption="";
           QString fileName = "";

           //if (parseMessagesRealtime){
                if( topic == "tns1:device/neutronprobing/detect" ){
                    eventCaption="Взрывчатые вещества";
                    messageProcessed=true;
                }
                if( topic == "tns1:device/introscopy/limitexceeded" ){
                    if( !QString(data["Picture"]).isEmpty()){

                        QString imgContent = data["Picture"];

                        QByteArray baimg = QByteArray::fromBase64(imgContent.toLatin1());
                        QTemporaryDir dir;
                        if (dir.isValid())
                        {

                            fileName = fileName.append(central->pathTemp);

                            qint32 n = (qrand() % ((900000 + 100000) - 100000) + 100000);
                            QString s = QString::number(n);
                            fileName = fileName.append(s);

                            QFile file(fileName);
                            if (!file.open(QIODevice::WriteOnly))
                            {
                                //FIXME:
                                // qDebug() << "ERROR: Can not write to file: ["<< fileName<< "]";
                            }
                            else
                            {
                                file.setPermissions(QFileDevice::ReadOwner | QFileDevice::WriteOwner);
                                file.write(baimg);
                                file.close();
                            }

                            central->log->msg("    изображение сохранено:" + fileName);
                            //emit showImageSignal(fileName);
                        }

                    }
                    eventCaption="Интроскопия";
                    messageProcessed=true;
                }
           //}


           if( messageProcessed ){
               k = QString::number(messagesCnt);

               message["name"] = k;
               message["serialNumber"]  = data["Id"];
               message["eventName"]     = "detect";
               message["eventCaption"]  = eventCaption;
               message["dateTime"]      = data["UtcTime"].replace("T"," ").replace("Z","").replace("-",".");
               message["message"]       = data["Comment"];
               message["message0"]      = data["Comment"];
               message["fileName"]      = fileName;
               message["device"]        = device;
               message["debug"]         = "";
               messages[k] = message;
               messagesCnt++;

               stateMessages = messagesCnt;

           }else{

               for (int i = 0; i < data.keys().size(); i++)
               {
                   QString key = data.keys()[i];
                   QString value = data[key];
                   central->log->msg("    "+key+"="+value);
               }
           }

       }

       QJsonDocument jdoc(messages);
       QString jstr(jdoc.toJson(QJsonDocument::Compact));
       emit updateMessagesSignal(jstr);

    }
}

QJsonObject Monitor::parseMessages(QJsonDocument r, bool archive)
{
    /*
        {
            "comment":"Нитроглицерин/Фенобарбитал",
            "explosiveType":"Explosives",
            "id":"001-00002",
            "location":"",
            "place":"",
            "utcTime":"2020-02-09T06:21:35"
        }
        KERBER
        {
           "event":"tns1:device/neutronprobing/detect",
           "message":[
              {
                 "comment":"Ацетилцеллюлоза",
                 "explosiveType":"Test",
                 "id":"001-00002",
                 "location":"",
                 "place":"",
                 "utcTime":"2020-02-09T06:21:34"
                }
            ],
           "messageAdvance":[
              {
                 "calibration":{
                    "negative":{
                       "calibrant":"CALN-PEAK-1",
                       "state":"Откалиброван"

                    },
                    "positive":{
                       "calibrant":"CALP-PEAK-1",
                       "state":"Откалиброван"

                    }

                 },
                 "devices":[
                    {
                       "dim":"",
                       "name":"Носик",
                       "value":"1.000"
                    },
                    {
                       "dim":"",
                       "name":"Салфетка",
                       "value":"0.000"
                    },
                    {
                       "dim":"%",
                       "name":"Вл.Вых",
                       "value":"0.600"
                    }
                ],
                 "info":{
                    "date":"16.05.2019",
                    "hostName":"030-2019T",
                    "polarity":"Dual",
                    "time":"15:36:11",
                    "type":"КЕРБЕР"
                },
                 "message":{
                },
                 "state":{
                    "id":"analyse",
                    "name":"Поиск непрерывно",
                    "tagValue":""
                },
                 "substances":[
                    {
                       "amplitude":"1102",
                       "comment":"Ацетилцеллюлоза",
                       "name":"AC",
                       "type":"Test"
                    }
                ]
            }
        ]
        }

        PARISCAN
        {
        "event":"tns1:device/introscopy/limitexceeded",
        "message":[],
        "messageAdvance":[{
            "fileName":"./ManualArchives/12345_20191018145153468_SCREENER.PNG",
            "id":"003-00001"}
        ]}

    */

    central->log->msg("        разбор сообщений");

    QJsonObject messages;
    QJsonObject message;
    QString k;
    QString msgText = "";
    QString anText = "";
    QString dateTime = "";
    QString eventCaption = "";
    QString fileName = "";
    QString debug = "";
    QString tpl = "";

    QJsonArray list = r["messageAdvance"].toArray();
    foreach (const QJsonValue &value, list)
    {

        msgText = "";
        anText = "";
        eventCaption = "";
        fileName = "";
        debug = "";

        QJsonObject container = value.toObject();
        QJsonObject element;

        if (archive)
        {
            element = container["json"].toObject();
        }
        else
        {
            element = container;
        }

        QJsonObject info = element["info"].toObject();

        QString serialNumber = "";

        if (archive)
        {
            serialNumber = container["serialNumber"].toString();
            dateTime = container["insertTime"].toString();

            debug = debug.append("id:");
            debug = debug.append(QString::number(container["id"].toInt()));
        }
        else
        {
            serialNumber = element["id"].toString();
            dateTime = info["date"].toString() + " " + info["time"].toString();
        }

        debug = debug.append("sn:");
        debug = debug.append(serialNumber);

        // central->log->msg("    device sn:"+serialNumber);

        QJsonObject device;

        if (devicesCnt > 0 && !serialNumber.isEmpty())
        {
            QJsonObject devicesList = devices;
            foreach (const QJsonValue &value, devicesList)
            {

                QJsonObject elementDevice = value.toObject();

                if (!elementDevice["serialNumber"].toString().isEmpty())
                {
                    if (elementDevice["serialNumber"].toString() == serialNumber)
                    {
                        device = elementDevice;
                    }
                }
            }
        }

        bool deviceProcessed = false;

        if (device["vendorId"].toInt() == DEVICE2_VENDOR_KERBER)
        {
            //central->log->msg("    kerber");
            /*
                 <substances>
                 <substance name="{%NAME_1%}" amplitude="{%INTENSITY_1%}" comment="{%COMMENT_1%}" type="{%TYPE_1%}"/>
                 {%NAME_1%} ({%COMMENT_1%}) [{%TYPE_1%}], интенсивность сигнала: {%INTENSITY_1%}
            */
            msgText = "Обнаруженные вещества:\n";
            anText = "";
            eventCaption = "Обнаружение";
            fileName = "";

            QJsonArray substancesList = element["substances"].toArray();
            foreach (const QJsonValue &value, substancesList)
            {

                QJsonObject elementSubstance = value.toObject();

                // central->log->msg("        substance:"+elementSubstance["comment"].toString());

                tpl = "%1 (%2) [%3], интенсивность сигнала:%4";
                tpl = tpl.arg(elementSubstance["name"].toString())
                          .arg(elementSubstance["comment"].toString())
                          .arg(elementSubstance["type"].toString())
                          .arg(elementSubstance["amplitude"].toString());
                msgText = msgText.append(tpl);

                tpl = "%1 (%2) [%3], %4";
                tpl = tpl.arg(elementSubstance["name"].toString())
                          .arg(elementSubstance["comment"].toString())
                          .arg(elementSubstance["type"].toString())
                          .arg(elementSubstance["amplitude"].toString());
                anText = anText.append(tpl);
            }
            deviceProcessed = true;
        }

        if (device["vendorId"].toInt() == DEVICE2_VENDOR_RAPISCAN)
        {
            //central->log->msg("    rapiscan");

            /*

                {
                    "deviceGroupId":3,
                    "eventName":"tns1:device/introscopy/limitexceeded",
                    "fileName":"/home/ubuntu/tmp/rapiscan-daemon/ManualArchives/12345_20020827215341562_1111.PNG",
                    "id":2619,"insertTime":"2020-02-16T16:12:42",
                    "json":{},
                    "raw":"278837",
                    "serialNumber":"003-00005",
                    "sstmkJson":{
                        "account":"","id":"003-00005",
                        "picture":"",
                        "result":"/home/ubuntu/tmp/rapiscan-daemon/ManualArchives/12345_20020827215341562_1111.PNG",
                        "utcTime":"2020-02-16T16:12:42"
                    }
                }
            */

            msgText = "";
            anText = "";
            eventCaption = "Обнаружение";

            if (archive)
            {
                QJsonObject sstmkJson = container["sstmkJson"].toObject();

                dateTime = sstmkJson["utcTime"].toString();
                fileName = sstmkJson["result"].toString();
            }
            else
            {
                // FIXME:
                dateTime = "09.02.2020 18:45:02";
                fileName = element["fileName"].toString();
            }

            debug = debug.append("file:");
            debug = debug.append(fileName);

            deviceProcessed = true;
        }

        if (deviceProcessed)
        {

            // k = element["id"].toString();
            k = QString::number(messagesCnt);

            message["name"] = k;
            message["serialNumber"] = serialNumber;
            message["eventName"] = "detect";
            message["eventCaption"] = eventCaption;
            message["dateTime"] = dateTime;
            message["message"] = msgText;
            message["message0"] = anText;
            message["fileName"] = fileName;
            message["anonce"] = anText;
            message["device"] = device;
            message["debug"] = debug;
            messages[k] = message;
            messagesCnt++;

            //central->log->msg("        sn:" + message["serialNumber"].toString() + " dt:" + message["dateTime"].toString());
        }

        stateMessages = messagesCnt;

        if (!deviceProcessed)
        {

            QString msg = "%1";
            msg = msg.arg(device["vendorId"].toInt());
            central->log->msg("    unprocessed device. vendorId=" + msg);
        }
    }

    return messages;
}

QJsonObject Monitor::getDeviceBySerialNumber(QString serialNumber){

    QJsonObject device;

    if (devicesCnt > 0 && !serialNumber.isEmpty())
    {
        QJsonObject devicesList = devices;
        foreach (const QJsonValue &value, devicesList)
        {

            QJsonObject elementDevice = value.toObject();

            if (!elementDevice["serialNumber"].toString().isEmpty())
            {
                if (elementDevice["serialNumber"].toString() == serialNumber)
                {
                    device = elementDevice;
                }
            }
        }
    }
    return device;
}

void Monitor::dumpMessages(QJsonObject messages)
{

    central->log->msg("    сообщения:");
    foreach (const QJsonValue &value, messages)
    {

        QJsonObject element = value.toObject();

        QString msg = "";
        msg = msg.append(" n=").append(element["name"].toString());
        msg = msg.append(" sn=").append(element["serialNumber"].toString());
        msg = msg.append(" dt=").append(element["dateTime"].toString());

        central->log->msg("        " + msg);
    }
}

void Monitor::nextQuery()
{
}




void Monitor::sendQuery(int queryType)
{
}

void Monitor::onSendQuery(QString &response)
{
}

void Monitor::logMessage(QString message)
{
    // central->log->msg(message);
    // message=message.append("\n--").append( central->log->getToday() ).append(" ").append(message).append("==");

    message = central->log->getToday() + " " + message;
    emit addMessage(message);
}

void Monitor::slotNewMessage(QHash<QString, QString> data)
{

    //central->log->msg("***** MONITOR NEW MESSAGE ******");

    if (!data.isEmpty())
    {
        //processResponseXml(data);

        /*
        //qDebug() << "***** MONITOR NEW MESSAGE ******";
        central->log->msg("***** MONITOR NEW MESSAGE ******");
        for (int i = 0; i < data.keys().size(); i++)
        {
            QString key = data.keys()[i];
            QString value = data[key];

            //qDebug() << "Key: " << key << ", Value: " << value;
            central->log->msg("    "+key+"="+value);
        }
        //qDebug() << "********************************";
        */

    }
}
