Для запуска готового теста:
    
    test.bat
    запустит 30 клиентов с 100 запросами для каждого
    результат в файле report.log

Для запуска теста с параметрами:
    
    test_users.bat server port client users requests
    Например:
    test_users.bat 192.168.11.254 8888 10.8.0.9 30 10
    Подсказка по клчам запуска:
    test_users.bat /?

