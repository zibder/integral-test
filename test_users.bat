@echo off
rem server port client users requests
setlocal enableDelayedExpansion

IF [%1]==[/?] goto :help

echo %* |find "/?" > nul
IF errorlevel 1 goto :main

:main
IF [%1] == [] goto :help
IF [%2] == [] goto :help
IF [%3] == [] goto :help
IF [%4] == [] goto :help
IF [%5] == [] goto :help

set server=%1
set port=%2
set client=%3
set /a users=%4
set /a requests=%5

del *.rlog>NUL
del *.run>NUL

echo Starting clients: !users! requests: !requests!

set /a base=20000
for /L %%i IN (1,1,%users%) do (
	set /a port=%%i+!base!	
	rem echo start /b test_users_thread.bat %server% %port% %client% !port! !requests! >> %t%
	start /b test_users_thread.bat %server% %port% %client% !port! !requests!
)

timeout 3 >NUL
echo|set /p="Testing"

:loop
if not exist *.run goto :next  	
	echo|set /p="."
    timeout 5 >NUL
goto loop

:next
timeout 3 >NUL
copy *.rlog  report.log 1>NUL
timeout 3 >NUL
echo All tests passed >> report.rlog 
type report.log 

echo All tests passed

echo See report.log fo details
del *.rlog 1>NUL
goto :end

:help
echo Usage:
echo test_users_thread.bat server port client users requests
goto :end

:end
pause